*exctracted files - README


-------------------------------------------------------------- ****/home --------------------------------------------------------------------------
Subfolders are named with participants' ID

--------> ID.mat 
Contains a "Data" structure with the variables extracted from each session (001 to 030)
seqType is the type of sequence presented during that particular trial (150 in total)
seqErrors tells how many mistakes were done during the execution of the sequence (it is the sum of the elements of numDispError computed over 10 consecutive elements with a step of 10) 
seqDurationAll is the MT for the execution of the sequence, accounting for all the errors
seqDuration is the MT for the execution of the sequence and is calculated w.r.t. the correct responses to display
numDispError tell us if at least one error was done in the display of the sequence
dispRep tell us how many times a particular display has been repeated
dispRT is the RT to the display, being it wrong or correct
dispError tells whether the response to the display was correct or not (1==correct; 0==wrong)

--------> ID_elaborated.mat
Contains home data elaborated to be analyzed and (if it is of interest) merged with laboratory data 

"Data" is the same data structure as in the ID.mat, but here the structure Data is updated with the variable dispNum which indicate the display number of the sequence that has being executed (from 1 to 10)

NB: Movement times are taken from seqDuration. If one want to use seqDurationAll, the elaboration script must rerun after rerunning the exctaction script with explicit reference to the wanted variable

Below are all cell vectors of dimension 6 x 1, with 6 == number of sequences ordered as EXT1, EXT2, MOD1, MOD2, MIN1, MIN2
- ERR:
Is a cell vector with the 6 sequences. The length of the double vector contained in each cell is the n° of trials (64*30 for EXT, 10*30 for MOD and 1*30 for MIN)
The vector is 1 if the sequence is wrong and 0 if it is correct
- ERRw: 
Values between 0 and 1, it indicates the proportion of wrong displays per sequence
- ERRt:
Indicates the absolute number of mistakes within a sequence
- MT:
Is a cell vector with the 6 sequences. The length of the double vector contained in each cell is the n° of trials (64*30 for EXT, 10*30 for MOD and 1*30 for MIN)
Movement time from first key press (1st display) to last key press (10th display)
Is seqDuration assembled from all the sessions. (One could decide to use seqDurationAll but this choice should be done during the exctraction phase)
- MTcorr:
Is a cell vector with the 6 sequences. The length of the double vector contained in each cell is the n° of trials (64*30 for EXT, 10*30 for MOD and 1*30 for MIN)
Movement time from first key press (1st display) to last key press (10th display), only for entirely correct sequences
- ORDERcorr:
same dimension as MTcorr but contains the indexes of the correct trials
- RT:
same dimension as MT, but this is the RT to the first display
- RTcorr:
same as RT, but for correct responses. The order is ORDERcorr






-------------------------------------------------------------- ****/lab --------------------------------------------------------------------------
Subfolders are named with participants' ID

--------> ID.mat 
Contains a "Data" structure with the variables extracted from each session (1 to 4). Might happen that one session is missing because the participant couldn't make it to come to the lab
Each session contains the data divided in 5 epochs: 60 trials (60 executed sequences) are executed in each epoch (10 EXT1, 10 EXT2, 10 MOD1, 10 MOD2, 10 MIN1, 10 MIN2). 
1)seqType (CELL) is the type of sequence presented during that particular trial (60 in total)
2)seqErrors (DOUBLE) tells how many mistakes were done during the execution of the sequence of the sequence
3)seqDurationAll (DOUBLE) is the MT for the execution of the sequence, accounting for all the errors
4)seqDuration (DOUBLE) is the MT for the execution of the sequence and is calculated w.r.t. the correct responses to display
5)RT (DOUBLE) is the RT to the display, being it wrong or correct
6)dispErrors (DOUBLE) tells the number of mistakes for a particular display: it is the same as numDispError for the home sessions.. -->> THERE IS A BIT OF CONFUSION HERE, MIGHT BE GOOD TO KEEP THE SAME TERMINOLOGY BETWEEN HOME AND LAB VARIABLES
 


--------> ID_elaborated.mat
Contains home data elaborated to be analyzed and (if it is of interest) merged with home data 

"Data" is the same data structure as in the ID.mat

NB: Movement times are taken from seqDuration. If one want to use seqDurationAll, the elaboration script must rerun after rerunning the exctaction script with explicit reference to the wanted variable

1) MTcorr (CELL) has size 5=epochs x 6=n_sequences x 4=n_sessions. Each cell element contains the Movement time of correct sequences (max 10). 
2) RTcorr (CELL) same as MTcorr but for RT to the first display of each correctly performed sequence
3) ORDERcorr (CELL) same as MTcorr. Indicates the index corresponding to the correctly performed sequences (from 1 to 10) 

The remaining content of the .mat file is composed by matlab tables. This choice was driven by the fact that in a subsequent script I used this tables to perform anovas and plotting things. Might not be the ideal choice w.r.t. what has been done on home data.. but so far it worked fine:
1) MT_Table_raw contains the MT for all the sequences. The row number (50) correspond to the (number of sequences of each type) per session, while the column number (24) to the number of different sequences (6) over sessions (4). 
   Example: the column corresponding to MOD1_s3 returns the MT for the 50 sequences MOD1 practiced during session 3. 
   NaN indicates that no data are available for that session
2) RT_Table_raw contains the RT for all the sequences. The row number (50) correspond to the (number of sequences of each type) per session, while the column number (24) to the number of different sequences (6) over sessions (4). 
   Example: the column corresponding to MOD1_s3 returns the RT for the 50 sequences MOD1 practiced during session 3. 
   NaN indicates that no data are available for that session
3) ERR_Table_raw contains the ERR for all the sequences (0 = correct, 1=at least one error). The row number (50) correspond to the (number of sequences of each type) per session, while the column number (24) to the number of different sequences (6) over sessions (4). 
   Example: the column corresponding to MOD1_s3 returns the ERR for the 50 sequences MOD1 practiced during session 3. 
   NaN indicates that no data are available for that session
4) ERR_Table_raw contains the number of wrong displays committed during the execution of each sequence (0 = all correct; 1= all 10 displays wrong). The row number (50) correspond to the (number of sequences of each type) per session, while the column number (24) to the number of different sequences (6) over sessions (4). 
   Example: the column corresponding to MOD1_s3 returns the number of wrong displays (between 0 and 1) for the 50 sequences MOD1 practiced during session 3. 
   NaN indicates that no data are available for that session

NB: Differently w.r.t. what has been done for home sessions, the absolute number of mistakes within a sequence was not elaborated. 

The script to elaborate the lab data already compute some descriptive statistics that is summarized in these tables
5) MT_table; 8 columns (from left to right): Scan session (from 1 to 4), Session epoch (from 1 to 5), mean MT for each sequence type (computed w.r.t. each epoch; EXT1 and EXT2, MOD1 and MOD2, MIN1 and MIN2 are put together to compute average), standard error of the mean (SEM) for each sequence type
             NaN indicates that no data are available for that session
6) RT_table; same as MT_table but for RT
7) ERR_Table; 7 columns (from left to right): Scan session (from 1 to 4), mean ERR for each sequence type (computed w.r.t. each session; EXT1 and EXT2, MOD1 and MOD2, MIN1 and MIN2 are put together to compute average), standard error of the mean (SEM) for each sequence type
8) ERRw_Table; same as ERR_Table, but for the % of wrong displays. 






-------------------------------------------------------------- ****/lab_home --------------------------------------------------------------------------
Subfolders are named with participants' ID


--------> ID_elaborated1.mat
Laboratory and home data are assembled together into 7 cell vectors

1) MT_HL (CELL) of size 6(n_sequences)x1; Each cell containts a double vector, the first cell being for EXT1 sequences (64 home sequences*30 home sessions + 50 sequences * 4 lab sessions == 2120) and the last for MIN2 sequences (1 home sequences*30 home sessions + 50 sequences * 4 lab sessions == 230).
   Trials are ordered by respecting the temporal sequence of the events: LAB1,HOME1-10,LAB2,HOME11-20,LAB3,HOME21-30,LAB4
 
2) RT_HL (CELL); same as MT_HL
3) ERR_HL (CELL); same as MT_HL
4) ERRw_HL (CELL); same as MT_HL
5) MTc_HL and RTc_HL; same as MT_HL, but for trials without errors in the sequence practice
6) ORDc_HL; same as MTc_HL but it contains the trial index of the correct responses (e.g., for EXT sequenced it goes from 1 to 2120)









