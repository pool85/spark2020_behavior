% Loads the common folder path used during the analysis
% 
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)

%% CONFIGURATION

% Configure here the folder paths for original data and results folders

% Original data folder
conf.RAW_FOLDER_HOME = ['..' filesep '..' filesep 'raw/home' ];
conf.EXTRACTED_FOLDER_HOME = [ '..' filesep '..' filesep 'extracted/home' ];

conf.RAW_FOLDER_LAB = ['..' filesep '..' filesep 'raw/lab' ];
conf.EXTRACTED_FOLDER_LAB = [ '..' filesep '..' filesep 'extracted/lab' ];

conf.EXTRACTED_FOLDER_LAB_HOME = [ '..' filesep '..' filesep 'extracted/lab_home' ];

conf.PLOT_FOLDER_LAB = [ '..' filesep '..' filesep 'plot/lab' ];
conf.PLOT_FOLDER_HOME = [ '..' filesep '..' filesep 'plot/home' ];
conf.PLOT_FOLDER_PRAC_INT = [ '..' filesep '..' filesep 'plot/prac_int' ];
conf.PLOT_FOLDER_FIT = [ '..' filesep '..' filesep 'plot/fit' ];

conf.RESULTS_FOLDER_FIT = [ '..' filesep '..' filesep 'results/fit' ];
%% AUTOMATICALLY GENERATED PATHS

% Results folder
%conf.PREPROCESS_FOLDER = [ conf.RESULTS_FOLDER filesep 'preprocessing' ];
%conf.ANALYSIS_FOLDER = [ conf.RESULTS_FOLDER filesep 'analysis' ];
