function conf = loadConfiguration()
% Loads the experimental setup configuration
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)


%
conf.Nseq = 6;

% 
conf.nsessLAB = 4;
conf.nsessHOME = 30;
conf.nsessHOMEinterLAB = 10;


%
conf.seq1 = 'EXT1';
conf.seq2 = 'EXT2';
conf.seq3 = 'MOD1';
conf.seq4 = 'MOD2';
conf.seq5 = 'MIN1';
conf.seq6 = 'MIN2';

%
conf.LAB_NSEQ_EPOCH = 10;

%
conf.LAB_Epochs = 5;
conf.LAB_Blocks = 6;

%
conf.LAB_EXT_trials = 50;
conf.LAB_MOD_trials = 50;
conf.LAB_MIN_trials = 50;

%
conf.HOME_EXT_trials = 64;
conf.HOME_MOD_trials = 10;
conf.HOME_MIN_trials = 1;


