function [ ] = plot_MT_RT_ERR_LAB(mean_ext,mean_mod,mean_min,sem_ext,sem_mod,sem_min,nsessions,axis_vector,title_fig,title_x,title_y,num_fig,type_behaviour)




if strcmp(type_behaviour,'ERR')
    
    
    
    
    % PLOT
    figure(num_fig)
    hold on
    axis(axis_vector)
    
    errorbar([1:nsessions]',mean_ext,sem_ext,'-s','MarkerSize',5,...
        'MarkerEdgeColor','r','MarkerFaceColor','r','Color','r');
    errorbar([1:nsessions]',mean_mod,sem_mod,'-s','MarkerSize',5,...
        'MarkerEdgeColor','k','MarkerFaceColor','k','Color','k');
    errorbar([1:nsessions]',mean_min,sem_min,'-s','MarkerSize',5,...
        'MarkerEdgeColor','b','MarkerFaceColor','b','Color','b');
    
    
    xticks(1:4)
    xticklabels({'1','2','3','4'})
    title(title_fig)
    xlabel(title_x)
    ylabel(title_y)
    legend({'EXT','MOD','MIN'})
    
    
    
    
else
    
    
    
    
    
    % PLOT
    figure(num_fig)
    hold on
    axis(axis_vector)
    
    if nsessions > 3
        
        errorbar([19:23]',mean_ext(16:end),sem_ext(16:end),'-s','MarkerSize',5,...
            'MarkerEdgeColor','r','MarkerFaceColor','r','Color','r');
        errorbar([19:23]',mean_mod(16:end),sem_mod(16:end),'-s','MarkerSize',5,...
            'MarkerEdgeColor','k','MarkerFaceColor','k','Color','k');
        errorbar([19:23]',mean_min(16:end),sem_min(16:end),'-s','MarkerSize',5,...
            'MarkerEdgeColor','b','MarkerFaceColor','b','Color','b');
        
    end
    
    if nsessions > 2
        
        errorbar([13:17]',mean_ext(11:15),sem_ext(11:15),'-s','MarkerSize',5,...
            'MarkerEdgeColor','r','MarkerFaceColor','r','Color','r');
        errorbar([13:17]',mean_mod(11:15),sem_mod(11:15),'-s','MarkerSize',5,...
            'MarkerEdgeColor','k','MarkerFaceColor','k','Color','k');
        errorbar([13:17]',mean_min(11:15),sem_min(11:15),'-s','MarkerSize',5,...
            'MarkerEdgeColor','b','MarkerFaceColor','b','Color','b');
        
    end
    
    if nsessions > 1
        
        errorbar([7:11]',mean_ext(6:10),sem_ext(6:10),'-s','MarkerSize',5,...
            'MarkerEdgeColor','r','MarkerFaceColor','r','Color','r');
        errorbar([7:11]',mean_mod(6:10),sem_mod(6:10),'-s','MarkerSize',5,...
            'MarkerEdgeColor','k','MarkerFaceColor','k','Color','k');
        errorbar([7:11]',mean_min(6:10),sem_min(6:10),'-s','MarkerSize',5,...
            'MarkerEdgeColor','b','MarkerFaceColor','b','Color','b');
        
    end
    
    if nsessions > 0
        
        errorbar([1:5]',mean_ext(1:5),sem_ext(1:5),'-s','MarkerSize',5,...
            'MarkerEdgeColor','r','MarkerFaceColor','r','Color','r');
        errorbar([1:5]',mean_mod(1:5),sem_mod(1:5),'-s','MarkerSize',5,...
            'MarkerEdgeColor','k','MarkerFaceColor','k','Color','k');
        errorbar([1:5]',mean_min(1:5),sem_min(1:5),'-s','MarkerSize',5,...
            'MarkerEdgeColor','b','MarkerFaceColor','b','Color','b');
        
    end
    
    xticks([1:5,7:11,13:17,19:24])
    xticklabels({'1','2','3','4','5','1','2','3','4','5','1','2','3','4','5','1','2','3','4','5'})
    title(title_fig)
    xlabel(title_x)
    ylabel(title_y)
    legend({'EXT','MOD','MIN'})
    
    
    
end


















end
