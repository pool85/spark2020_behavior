function [vector_out] = averageBIN(vector_in,binsize)
%
% SPARK PROJECT 2020.
%
% This function average a vector according to the binsize, without
% overlapping. 
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)

cont = 0;
i_end = 0;
while (length(vector_in)-i_end) >= binsize

    cont = cont + 1;
    
    i_in = (cont-1)*binsize+1;
    i_end = cont*binsize;

    vector_out(cont,1) = mean(vector_in(i_in:i_end));

end



end

