function [ ] = plot_MT_RT_ERR_LAB_HOME(mean_1,mean_2,mean_3,sem_1,sem_2,sem_3,nbins,bin_size,axis_vector,title_fig,title_x,title_y,num_fig,text_legend)

% TO DO: Explain function




% PLOT
figure(num_fig)
hold on
axis(axis_vector)


if ~isempty(mean_1)    
    errorbar([1:nbins]',mean_1,sem_1,'-s','MarkerSize',5,...
        'MarkerEdgeColor','r','MarkerFaceColor','r','Color','r')    
end

if ~isempty(mean_2)
    errorbar([1:nbins]',mean_2,sem_2,'-s','MarkerSize',5,...
        'MarkerEdgeColor','k','MarkerFaceColor','k','Color','k');
end

if ~isempty(mean_3)
    errorbar([1:nbins]',mean_3,sem_3,'-s','MarkerSize',5,...
        'MarkerEdgeColor','b','MarkerFaceColor','b','Color','b');
end


xticks(1:nbins)

for i = 1:nbins
    var_xticklabels{1,i} = [num2str(i*bin_size)];
end

xticklabels(var_xticklabels)


title(title_fig)
xlabel(title_x)
ylabel(title_y)
legend(text_legend)



end
