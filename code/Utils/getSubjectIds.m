function [ids,sbjids] = getSubjectIds( folder )
% Reads the subjects ids from a data folder
% Only subject ids with 5 letters are considered
%
% Usage:
%   [ ids ] = getSubjectIds( folder )
%
% Input parameters:
%   folder     ... The EEG data folder
%
% Output variables:
%   ids       ... A cell array containing the subject IDs
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)

listing = dir(folder);

ids = {};
sbjids = {};
for i = 1:length(listing)
    [~ , name, ext] = fileparts(listing(i).name);
    if strcmp(ext, '.csv')
        ids = [ ids name ];
        sbjids = [sbjids name(1:5)];
    end
end
sbjids = unique(sbjids);




end