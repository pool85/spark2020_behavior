function [sequence_index] = seqOrder_HOME(nTrials,TrialsPerSequence,nSess,trialsType,seqName1,seqName2,seqName3,seqName4,seqName5,seqName6)
% Order the sequences in order of display during. Each sequence type is
% ordered according to his own order (from 1 to TrialsPerSequence)
% 
% INPUTS:
% nTrials: number of trials in the vector (e.g., one epoch of a scan
%          session has 60 trials
% TrialsPerSequence is a vector describing the number of trials per
%          sequence (first element is the first sequence: e.g. 'EXT1') 
% nSess is the number of the session. In this way, the sequences are numbered
%          by taking into account the sequence order (e.g.: session 1 EXT1
%          are ordered from 1:64, while between 65:128 in session 2)
% trialsType is the vector containing the order of the presented sequences
%          (e.g., Data.(session_str).(epoch_str).seqType)
% seqName1-6 is the name of the sequence 
%
% OUTPUT:
% sequence_index is a vector of length nTrials, containing the sequential order of the sequences 
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)

sequence_index = zeros(nTrials,1);
sequence_index(contains(trialsType,seqName1)) = [(nSess-1)*TrialsPerSequence(1) + 1 : (nSess)*TrialsPerSequence(1) ]';
sequence_index(contains(trialsType,seqName2)) = [(nSess-1)*TrialsPerSequence(2) + 1 : (nSess)*TrialsPerSequence(2) ]';
sequence_index(contains(trialsType,seqName3)) = [(nSess-1)*TrialsPerSequence(3) + 1 : (nSess)*TrialsPerSequence(3) ]';
sequence_index(contains(trialsType,seqName4)) = [(nSess-1)*TrialsPerSequence(4) + 1 : (nSess)*TrialsPerSequence(4) ]';
sequence_index(contains(trialsType,seqName5)) = [(nSess-1)*TrialsPerSequence(5) + 1 : (nSess)*TrialsPerSequence(5) ]';
sequence_index(contains(trialsType,seqName6)) = [(nSess-1)*TrialsPerSequence(6) + 1 : (nSess)*TrialsPerSequence(6) ]';


end

