function [sequence_index] = seqOrder_LAB(nTrials,TrialsPerSequence,trialsType,seqName1,seqName2,seqName3,seqName4,seqName5,seqName6)
% Order the sequences in order of display during. Each sequence type is
% ordered according to his own order (from 1 to TrialsPerSequence)
% 
% INPUTS:
% nTrials: number of trials in the vector (e.g., one epoch of a scan
%          session has 60 trials
% trialsType is the vector containing the order ofthe presented sequences
%          (e.g., Data.(session_str).(epoch_str).seqType)
% seqName1-6 is the name of the sequence 
%
% OUTPUT:
% sequence_index is a vector of length nTrials, containing the sequential order of the sequences 
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)

sequence_index = zeros(nTrials,1);
sequence_index(contains(trialsType,seqName1)) = [1:TrialsPerSequence]';
sequence_index(contains(trialsType,seqName2)) = [1:TrialsPerSequence]';
sequence_index(contains(trialsType,seqName3)) = [1:TrialsPerSequence]';
sequence_index(contains(trialsType,seqName4)) = [1:TrialsPerSequence]';
sequence_index(contains(trialsType,seqName5)) = [1:TrialsPerSequence]';
sequence_index(contains(trialsType,seqName6)) = [1:TrialsPerSequence]';


end

