function createFolderPath( f )
% Creates the path to the specified file f, if the folder structure does not exist
%
% Usage:
%   createFolderPath( f )
%
% Input parameters:
%   f     ... Filename
%   
%                             
% Copyright (c) 2017 Intento SA
% Andrea Maesani (andrea.maesani@intento.ch)

[filepath, ~, ~] = fileparts(f);
if ~exist(filepath, 'dir')
   mkdir(filepath); 
end