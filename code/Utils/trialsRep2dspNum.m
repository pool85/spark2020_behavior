function [dsp] = trialsRep2dspNum(TrialsRep,numDSP)
% SPARK PROJECT 2020
%
% This function compute the display number of the HOME sessions from
% information regarding display repetitions
%
% - TrialsRep is a vector containing any number between 0 and Inf. First
% apparition of the display is 0, then 1 onwards when the subject makes
% incorrect response to the display
%
% - numDSP is the number of displays contained in each sequence. SPARK task
% 2020 contains 10 displays. 
%
% - dsp is a vector of the same size as TrialsRep, containing information
% regarding the number of the corresponding display. This is useful to
% identify RT to the first display of each sequence. 
%
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)

cont_dsp = 0; 

for i = 1:length(TrialsRep)
    
    if and(mod(cont_dsp,numDSP)==0 ,TrialsRep(i)==0) 
        cont_dsp = 0;
    end
    
    
    if TrialsRep(i) == 0
        cont_dsp = cont_dsp + 1;
        dsp(i) = cont_dsp;
    else
        dsp(i) = cont_dsp;
    end
    
    
end
end

