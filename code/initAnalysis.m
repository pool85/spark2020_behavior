% Initialization script
% 
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)

% Scripts folder
configurationFolder = [ '..' filesep 'Configuration' ];
preprocessingFolder = [ '..' filesep 'Preprocessing' ];
utilsFolder = ['..' filesep 'Utils' ];

% Add paths
addpath( configurationFolder );
addpath( preprocessingFolder );
addpath( utilsFolder );

conf = loadConfiguration();
loadFolderPaths;



