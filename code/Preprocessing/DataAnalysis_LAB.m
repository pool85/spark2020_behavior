% DataAnalysis_LAB.m
%
% SPARK PROJECT 2020.
%
% This script performs the behavioural analysis of the MT, RT and ERR
% extraced and elaborated from the laboratory sessions. For the moment, we
% test baseline performance during pre-training, with rm anova on the
% dependent variables MT, RT and ERR.
%
% TO DO: For the moment, the code does the analysis according to the common
% number of LAB sessions across participants. At some point, we should
% distinguish between an analysis done on the PRE-training sessions (factor
% PRACTICE and EPOCHS), from an analysis done on the training scans 1 to 3
% (see Wymbs 2015) 
%
%
% The script also plots the following:
% - MR and RT: The data of each subject, detailed in terms of eoochs, practice and
% sessions
% - ERR: The data of each subject are detailed in terms of practice and
% sessions
% - The Grand average is plotted for the sessions that are completed by all
% the participants.
%
%
%
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)
clc
clear all

%% Initialize the script
addpath('..');
initAnalysis

%% options
saveflag = 0; % 1: save figure plots; 0: don't save
figflag = 1;  % 1: plot figure; 0: don't plot

%% Find file names and subject IDs
[~ , subjects] = getSubjectIds(conf.RAW_FOLDER_LAB);

%% Eclude subjects from analysis
vec_ex = [1,3,5,8,10,14,15,16,18];
vec_ex = [1,3,5,8,10,14,15,16,18];
%vec_ex = [];

ID_exclude ={};
for i=vec_ex
    
    ID_exclude = [ID_exclude,subjects{i}]; % ID of participants to exclude for the moment (e.g., participants that just started the protocol)
    
end

if ~isempty(ID_exclude)
    for i = 1:length(ID_exclude)
        subjects(contains(subjects,ID_exclude{i})) = [];
    end
end

nSubjects = length(subjects); % number of subjects

%% Loop over the subjects to see which is the maximum common number of LAB sessions performed by each participant
[csvFileNames, ~] = getSubjectIds(conf.RAW_FOLDER_LAB);

for i = 1:nSubjects
    
    SubjectsId = subjects{i};
    
    Sessions_LAB = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions_LAB = length(Sessions_LAB);
    
    SbjSessions_LAB(i) = nSessions_LAB;
    
end  % end loop over subjects

MaxSessionsLAB = min(SbjSessions_LAB);

disp(['The maximum common number of sessions performed by each participant is ',num2str(MaxSessionsLAB)])


%% start analysis
for i = 1:nSubjects
    
    
    SubjectsId = subjects{i};
    
    Sessions_LAB = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions_LAB = length(Sessions_LAB);
    
    inputDataset  = [ conf.EXTRACTED_FOLDER_LAB filesep SubjectsId filesep SubjectsId '_elaborated.mat' ];
    
    %% load data and keep common practiced sessions
    load(inputDataset,'MT_Table','RT_Table','ERR_Table','ERRw_Table');
    
    % Get MT data for the common practiced sessions
    tmp_MT_EXT = MT_Table.MT_EXT_MEAN;
    tmp_MT_MOD = MT_Table.MT_MOD_MEAN;
    tmp_MT_MIN = MT_Table.MT_MIN_MEAN;
    MT_EXT(:,i) = tmp_MT_EXT(1:conf.LAB_Epochs*MaxSessionsLAB);
    MT_MOD(:,i) = tmp_MT_MOD(1:conf.LAB_Epochs*MaxSessionsLAB);
    MT_MIN(:,i) = tmp_MT_MIN(1:conf.LAB_Epochs*MaxSessionsLAB);
    
    % Get RT data for the common practiced sessions
    tmp_RT_EXT = RT_Table.RT_EXT_MEAN;
    tmp_RT_MOD = RT_Table.RT_MOD_MEAN;
    tmp_RT_MIN = RT_Table.RT_MIN_MEAN;
    RT_EXT(:,i) = tmp_RT_EXT(1:conf.LAB_Epochs*MaxSessionsLAB);
    RT_MOD(:,i) = tmp_RT_MOD(1:conf.LAB_Epochs*MaxSessionsLAB);
    RT_MIN(:,i) = tmp_RT_MIN(1:conf.LAB_Epochs*MaxSessionsLAB);
    
    % Get ERR data for the common practiced sessions
    tmp_ERR_EXT = ERR_Table.ERR_EXT_MEAN;
    tmp_ERR_MOD = ERR_Table.ERR_MOD_MEAN;
    tmp_ERR_MIN = ERR_Table.ERR_MIN_MEAN;
    ERR_EXT(:,i) = tmp_ERR_EXT(1:MaxSessionsLAB);
    ERR_MOD(:,i) = tmp_ERR_MOD(1:MaxSessionsLAB);
    ERR_MIN(:,i) = tmp_ERR_MIN(1:MaxSessionsLAB);
    
    % Get ERRw data for the common practiced sessions
    tmp_ERRw_EXT = ERRw_Table.ERRw_EXT_MEAN;
    tmp_ERRw_MOD = ERRw_Table.ERRw_MOD_MEAN;
    tmp_ERRw_MIN = ERRw_Table.ERRw_MIN_MEAN;
    ERRw_EXT(:,i) = tmp_ERRw_EXT(1:MaxSessionsLAB);
    ERRw_MOD(:,i) = tmp_ERRw_MOD(1:MaxSessionsLAB);
    ERRw_MIN(:,i) = tmp_ERRw_MIN(1:MaxSessionsLAB);
    
    %% plot data for each subject
    
    % plot
    if figflag
        
        % MT
        plot_MT_RT_ERR_LAB(MT_Table.MT_EXT_MEAN,MT_Table.MT_MOD_MEAN,MT_Table.MT_MIN_MEAN,MT_Table.MT_EXT_SEM,MT_Table.MT_MOD_SEM,MT_Table.MT_MIN_SEM,...
            nSessions_LAB,[0 24 0 9],['Movement time; Subject ID: ',SubjectsId],'Scan Sessions (5 Epochs / Session)','Movement Time [seconds]',100+i,'MT');
        
        % RT
        plot_MT_RT_ERR_LAB(RT_Table.RT_EXT_MEAN,RT_Table.RT_MOD_MEAN,RT_Table.RT_MIN_MEAN,RT_Table.RT_EXT_SEM,RT_Table.RT_MOD_SEM,RT_Table.RT_MIN_SEM,...
            nSessions_LAB,[0 24 0 1.4],['Response Time; Subject ID: ',SubjectsId],'Scan Sessions (5 Epochs / Session)','Response Time [seconds]',200+i,'RT');
        
        % ERR
        plot_MT_RT_ERR_LAB(ERR_Table.ERR_EXT_MEAN,ERR_Table.ERR_MOD_MEAN,ERR_Table.ERR_MIN_MEAN,ERR_Table.ERR_EXT_SEM,ERR_Table.ERR_MOD_SEM,ERR_Table.ERR_MIN_SEM,...
            nSessions_LAB,[0 5 0 1],['Error rate; Subject ID: ',SubjectsId],'Scan Sessions','Error rate',300+i,'ERR');
        
        % ERRw
        plot_MT_RT_ERR_LAB(ERRw_Table.ERRw_EXT_MEAN,ERRw_Table.ERRw_MOD_MEAN,ERRw_Table.ERRw_MIN_MEAN,ERRw_Table.ERRw_EXT_SEM,ERRw_Table.ERRw_MOD_SEM,ERRw_Table.ERRw_MIN_SEM,...
            nSessions_LAB,[0 5 0 0.3],['Error rate weighted; Subject ID: ',SubjectsId],'Scan Sessions','Error rate weighted',400+i,'ERR');
    end
    
    % print figures
    if saveflag
        
        saveas(figure(100+i),[conf.PLOT_FOLDER_LAB filesep 'ID_' SubjectsId '_MovTime' '.png'])
        saveas(figure(200+i),[conf.PLOT_FOLDER_LAB filesep 'ID_' SubjectsId '_ResTime' '.png'])
        saveas(figure(300+i),[conf.PLOT_FOLDER_LAB filesep 'ID_' SubjectsId '_ErrRate' '.png'])
        saveas(figure(400+i),[conf.PLOT_FOLDER_LAB filesep 'ID_' SubjectsId '_ErrRateWeighted' '.png'])
        
    end
    
    
end  % end loop over subjects


%% rm ANOVA on the pre-training session. MT & RT -> Factors: PRACTICE (3 levels) and EPOCHS (5 levels) ERR -> Factors: PRACTICE (3 levels)

% 1) MT

% prepare data for anova table
temp_EXT = MT_EXT';
temp_MOD = MT_MOD';
temp_MIN = MT_MIN';

% prepare varnames
F1 = {'EXT','MOD','MIN'}; % levels of factor 1
F2 = cell(1,conf.LAB_Epochs); % prepare levels of factor 2
for nlevF2 = 1:conf.LAB_Epochs
    F2{nlevF2} = ['E',num2str(nlevF2)];
end

% if we have more than one session, we have to add one factor for ANOVA
if MaxSessionsLAB > 1
    F3 = cell(1,MaxSessionsLAB); % prepare levels of factor 3
    for nlevF3 = 1:MaxSessionsLAB
        F3{nlevF3} = ['S',num2str(nlevF3)];
    end
end

% name of variables
if MaxSessionsLAB == 1
    cont = 0;
    for nlevF1 = 1:length(F1)
        for nlevF2 = 1:length(F2)
            
            cont = cont +1;
            varnames{1,cont} = [F1{nlevF1},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
            
        end
    end
    
else
    
    cont = 0;
    for nlevF1 = 1:length(F1)
        for nlevF3 = 1:length(F3)
            for nlevF2 = 1:length(F2)
                
                cont = cont +1;
                varnames{1,cont} = [F1{nlevF1},'_',F3{nlevF3},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
                
            end
        end
    end
    
end


% prepare table with the data for the rm anova
t_MT = array2table([temp_EXT,temp_MOD,temp_MIN],'VariableNames',varnames);

% this is the structure of the within subject factors
if MaxSessionsLAB == 1
    
    structure_practice = reshape(repmat((1:length(F1))',1,length(F2))',1,length(F1)*length(F2));
    structure_epoch = repmat((1:length(F2)),1,length(F1));
    
else
    
    structure_practice = reshape(repmat((1:length(F1))',1,length(F2)*length(F3))',1,length(F1)*length(F2)*length(F3));
    structure_epoch = repmat((1:length(F2)),1,length(F1)*length(F3));
    structure_session = repmat(reshape(repmat((1:length(F3))',1,length(F2))',1,length(F2)*length(F3)),1,length(F1));
    
end

if MaxSessionsLAB == 1
    
    WithinStructure_MT = table(categorical(structure_practice)',categorical(structure_epoch)','VariableNames',{'Practice','Epoch'});
    
    rm_MT = fitrm(t_MT,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_MT,'WithinModel','Practice*Epoch');
    
    ranovatable_MT = ranova(rm_MT,'WithinModel','Practice*Epoch');
    
else
    
    WithinStructure_MT = table(categorical(structure_practice)',categorical(structure_session)',categorical(structure_epoch)','VariableNames',{'Practice','Session','Epoch'});
    
    rm_MT = fitrm(t_MT,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_MT,'WithinModel','Practice*Session*Epoch');
    
    ranovatable_MT = ranova(rm_MT,'WithinModel','Practice*Session*Epoch');
    
end

clear temp_EXT temp_MOD temp_MIN varnames


% 2) RT

% prepare data for anova table
temp_EXT = RT_EXT';
temp_MOD = RT_MOD';
temp_MIN = RT_MIN';

% prepare varnames
F1 = {'EXT','MOD','MIN'}; % levels of factor 1
F2 = cell(1,conf.LAB_Epochs); % prepare levels of factor 2
for nlevF2 = 1:conf.LAB_Epochs
    F2{nlevF2} = ['E',num2str(nlevF2)];
end

% if we have more than one session, we have to add one factor for ANOVA
if MaxSessionsLAB > 1
    F3 = cell(1,MaxSessionsLAB); % prepare levels of factor 3
    for nlevF3 = 1:MaxSessionsLAB
        F3{nlevF3} = ['S',num2str(nlevF3)];
    end
end

% name of variables
if MaxSessionsLAB == 1
    cont = 0;
    for nlevF1 = 1:length(F1)
        for nlevF2 = 1:length(F2)
            
            cont = cont +1;
            varnames{1,cont} = [F1{nlevF1},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
            
        end
    end
    
else
    
    cont = 0;
    for nlevF1 = 1:length(F1)
        for nlevF3 = 1:length(F3)
            for nlevF2 = 1:length(F2)
                
                cont = cont +1;
                varnames{1,cont} = [F1{nlevF1},'_',F3{nlevF3},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
                
            end
        end
    end
    
end


% prepare table with the data for the rm anova
t_RT = array2table([temp_EXT,temp_MOD,temp_MIN],'VariableNames',varnames);

% this is the structure of the within subject factors
if MaxSessionsLAB == 1
    
    structure_practice = reshape(repmat((1:length(F1))',1,length(F2))',1,length(F1)*length(F2));
    structure_epoch = repmat((1:length(F2)),1,length(F1));
    
else
    
    structure_practice = reshape(repmat((1:length(F1))',1,length(F2)*length(F3))',1,length(F1)*length(F2)*length(F3));
    structure_epoch = repmat((1:length(F2)),1,length(F1)*length(F3));
    structure_session = repmat(reshape(repmat((1:length(F3))',1,length(F2))',1,length(F2)*length(F3)),1,length(F1));
    
end

if MaxSessionsLAB == 1
    
    WithinStructure_RT = table(categorical(structure_practice)',categorical(structure_epoch)','VariableNames',{'Practice','Epoch'});
    
    rm_RT = fitrm(t_RT,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_RT,'WithinModel','Practice*Epoch');
    
    ranovatable_RT = ranova(rm_RT,'WithinModel','Practice*Epoch');
    
else
    
    WithinStructure_RT = table(categorical(structure_practice)',categorical(structure_session)',categorical(structure_epoch)','VariableNames',{'Practice','Session','Epoch'});
    
    rm_RT = fitrm(t_RT,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_RT,'WithinModel','Practice*Session*Epoch');
    
    ranovatable_RT = ranova(rm_RT,'WithinModel','Practice*Session*Epoch');
    
end

clear temp_EXT temp_MOD temp_MIN varnames


% 3) ERR

% prepare data for anova table
temp_EXT = ERR_EXT';
temp_MOD = ERR_MOD';
temp_MIN = ERR_MIN';

% prepare varnames
F1 = {'EXT','MOD','MIN'}; % levels of factor 1
F2 = cell(1,MaxSessionsLAB); % prepare levels of factor 2
for nlevF2 = 1:MaxSessionsLAB
    F2{nlevF2} = ['S',num2str(nlevF2)];
end

cont = 0;
for nlevF1 = 1:length(F1)
    for nlevF2 = 1:length(F2)
        
        cont = cont +1;
        varnames{1,cont} = [F1{nlevF1},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
        
    end
end

% prepare table with the data for the rm anova
t_ERR = array2table([temp_EXT,temp_MOD,temp_MIN],'VariableNames',varnames);

% this is the structure of the within subject factors
if MaxSessionsLAB == 1
    
    structure_practice = 1:length(F1);
    
else
    
    structure_practice = reshape(repmat((1:length(F1))',1,length(F2))',1,length(F1)*length(F2));
    structure_session = repmat((1:length(F2)),1,length(F1));
    
end

% this is the structure of the within subject factors
if MaxSessionsLAB == 1
    
    WithinStructure_ERR = table(categorical(structure_practice)','VariableNames',{'Practice'});
    
    rm_ERR = fitrm(t_ERR,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_ERR,'WithinModel','Practice');
    
    ranovatable_ERR = ranova(rm_ERR,'WithinModel','Practice');
    
else
    
    WithinStructure_ERR = table(categorical(structure_practice)',categorical(structure_session)','VariableNames',{'Practice','Session'});
    
    rm_ERR = fitrm(t_ERR,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_ERR,'WithinModel','Practice*Session');
    
    ranovatable_ERR = ranova(rm_ERR,'WithinModel','Practice*Session');
    
end

clear temp_EXT temp_MOD temp_MIN varnames


% 4) ERRw

% prepare data for anova table
temp_EXT = ERRw_EXT';
temp_MOD = ERRw_MOD';
temp_MIN = ERRw_MIN';

% prepare varnames
F1 = {'EXT','MOD','MIN'}; % levels of factor 1
F2 = cell(1,MaxSessionsLAB); % prepare levels of factor 2
for nlevF2 = 1:MaxSessionsLAB
    F2{nlevF2} = ['S',num2str(nlevF2)];
end

cont = 0;
for nlevF1 = 1:length(F1)
    for nlevF2 = 1:length(F2)
        
        cont = cont +1;
        varnames{1,cont} = [F1{nlevF1},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
        
    end
end

% prepare table with the data for the rm anova
t_ERRw = array2table([temp_EXT,temp_MOD,temp_MIN],'VariableNames',varnames);

% this is the structure of the within subject factors
if MaxSessionsLAB == 1
    
    structure_practice = 1:length(F1);
    
else
    
    structure_practice = reshape(repmat((1:length(F1))',1,length(F2))',1,length(F1)*length(F2));
    structure_session = repmat((1:length(F2)),1,length(F1));
    
end

% this is the structure of the within subject factors
if MaxSessionsLAB == 1
    
    WithinStructure_ERRw = table(categorical(structure_practice)','VariableNames',{'Practice'});
    
    rm_ERRw = fitrm(t_ERRw,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_ERRw,'WithinModel','Practice');
    
    ranovatable_ERRw = ranova(rm_ERRw,'WithinModel','Practice');
    
else
    
    WithinStructure_ERRw = table(categorical(structure_practice)',categorical(structure_session)','VariableNames',{'Practice','Session'});
    
    rm_ERRw = fitrm(t_ERRw,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_ERRw,'WithinModel','Practice*Session');
    
    ranovatable_ERRw = ranova(rm_ERRw,'WithinModel','Practice*Session');
    
end

clear temp_EXT temp_MOD temp_MIN varnames

%% compute average and SEM over subjects

% MT
MT_EXT_mean_sbj = mean(MT_EXT,2);
MT_MOD_mean_sbj = mean(MT_MOD,2);
MT_MIN_mean_sbj = mean(MT_MIN,2);
MT_EXT_SEM_sbj = std(MT_EXT,0,2)/sqrt(size(MT_EXT,2));
MT_MOD_SEM_sbj = std(MT_MOD,0,2)/sqrt(size(MT_MOD,2));
MT_MIN_SEM_sbj = std(MT_MIN,0,2)/sqrt(size(MT_MIN,2));

% RT
RT_EXT_mean_sbj = mean(RT_EXT,2);
RT_MOD_mean_sbj = mean(RT_MOD,2);
RT_MIN_mean_sbj = mean(RT_MIN,2);
RT_EXT_SEM_sbj = std(RT_EXT,0,2)/sqrt(size(RT_EXT,2));
RT_MOD_SEM_sbj = std(RT_MOD,0,2)/sqrt(size(RT_MOD,2));
RT_MIN_SEM_sbj = std(RT_MIN,0,2)/sqrt(size(RT_MIN,2));

% ERR
ERR_EXT_mean_sbj = mean(ERR_EXT,2);
ERR_MOD_mean_sbj = mean(ERR_MOD,2);
ERR_MIN_mean_sbj = mean(ERR_MIN,2);
ERR_EXT_SEM_sbj = std(ERR_EXT,0,2)/sqrt(size(ERR_EXT,2));
ERR_MOD_SEM_sbj = std(ERR_MOD,0,2)/sqrt(size(ERR_MOD,2));
ERR_MIN_SEM_sbj = std(ERR_MIN,0,2)/sqrt(size(ERR_MIN,2));

% ERRw
ERRw_EXT_mean_sbj = mean(ERRw_EXT,2);
ERRw_MOD_mean_sbj = mean(ERRw_MOD,2);
ERRw_MIN_mean_sbj = mean(ERRw_MIN,2);
ERRw_EXT_SEM_sbj = std(ERRw_EXT,0,2)/sqrt(size(ERRw_EXT,2));
ERRw_MOD_SEM_sbj = std(ERRw_MOD,0,2)/sqrt(size(ERRw_MOD,2));
ERRw_MIN_SEM_sbj = std(ERRw_MIN,0,2)/sqrt(size(ERRw_MIN,2));


%% plot average data

if figflag

    % MT
    plot_MT_RT_ERR_LAB(MT_EXT_mean_sbj,MT_MOD_mean_sbj,MT_MIN_mean_sbj,MT_EXT_SEM_sbj,MT_MOD_SEM_sbj,MT_MIN_SEM_sbj,...
        MaxSessionsLAB,[0 24 0 9],['Movement time; GRAND Average (',num2str(nSubjects),' subjects)'],'Scan Sessions (5 Epochs / Session)','Movement Time [seconds]',10,'MT');

    % RT
    plot_MT_RT_ERR_LAB(RT_EXT_mean_sbj,RT_MOD_mean_sbj,RT_MIN_mean_sbj,RT_EXT_SEM_sbj,RT_MOD_SEM_sbj,RT_MIN_SEM_sbj,...
        MaxSessionsLAB,[0 24 0 1.4],['Response time; GRAND Average (',num2str(nSubjects),' subjects)'],'Scan Sessions (5 Epochs / Session)','Response Time [seconds]',20,'RT');
    
    % ERR
    plot_MT_RT_ERR_LAB(ERR_EXT_mean_sbj,ERR_MOD_mean_sbj,ERR_MIN_mean_sbj,ERR_EXT_SEM_sbj,ERR_MOD_SEM_sbj,ERR_MIN_SEM_sbj,...
        MaxSessionsLAB,[0 5 0 1],['Error rate; GRAND Average (',num2str(nSubjects),' subjects)'],'Scan Sessions','Error rate',30,'ERR');
    
    % ERRw
    plot_MT_RT_ERR_LAB(ERRw_EXT_mean_sbj,ERRw_MOD_mean_sbj,ERRw_MIN_mean_sbj,ERRw_EXT_SEM_sbj,ERRw_MOD_SEM_sbj,ERRw_MIN_SEM_sbj,...
        MaxSessionsLAB,[0 5 0 1],['Error rate weighted; GRAND Average (',num2str(nSubjects),' subjects)'],'Scan Sessions','Error rate weighted',40,'ERR');

end

%% print figures

if saveflag
    saveas(figure(10),[conf.PLOT_FOLDER_LAB filesep 'GrandAverage_MovTime' '.png'])
    saveas(figure(20),[conf.PLOT_FOLDER_LAB filesep 'GrandAverage_ResTime' '.png'])
    saveas(figure(30),[conf.PLOT_FOLDER_LAB filesep 'GrandAverage_ErrRate' '.png'])
    saveas(figure(40),[conf.PLOT_FOLDER_LAB filesep 'GrandAverage_ErrRateWeighted' '.png'])
end





