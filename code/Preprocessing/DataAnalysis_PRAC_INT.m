% DataAnalysis_PRAC_INT.m
%
% SPARK PROJECT 2020.
%
% This script performs an analysis to investigate the effect of practice
% intensity on learning. To do this, we consider both home- and lab-based
% training data, and compare performance data for each exposure condition.
%
% % TODO:  - Add plot for single subject? Maybe without error bars. 
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)
clear all
close all
clc

%% Initialize the script
addpath('..');
initAnalysis

%% Options
saveflag = 0; % 1: save figs; 0: don't save
ID_exclude = {}; % ID of participants to exclude for the moment (e.g., participants that just started the protocol)
MaxSessionsLAB = 1; % because of COVID-19, we could only LAB-scan all the subjects once. This will change once we add participants with multiple scans
BIN_SIZE = 50;



%% Find file names and subject IDs (from HOME folder)
[csvFileNames, subjects] = getSubjectIds(conf.RAW_FOLDER_HOME);

if ~isempty(ID_exclude)
    for i = 1:length(ID_exclude)
        subjects(contains(subjects,ID_exclude{i})) = [];
    end
end

nSubjects = length(subjects); % number of subjects


%% Loop over the subjects to see which is the maximum common number of HOME-based sessions performed by each participant

for i = 1:nSubjects
    
    SubjectsId = subjects{i};
    
    Sessions_HOME = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions_HOME = length(Sessions_HOME);
    
    SbjSessions_HOME(i) = nSessions_HOME;
    
end  % end loop over subjects

MaxSessionsHOME = min(SbjSessions_HOME);

disp(['The maximum common number of HOME-based sessions performed by each participant is ',num2str(MaxSessionsHOME)])



%% Start Analysis

for i = 1:nSubjects
    
    SubjectsId = subjects{i};
        
    inputDatasetLAB_HOME = [ conf.EXTRACTED_FOLDER_LAB_HOME filesep SubjectsId filesep SubjectsId '_elaborated1.mat' ];
    
    
    %% load data elaborated from HOME and LAB sessions
    load(inputDatasetLAB_HOME,'MT_HL','RT_HL','ERR_HL','ERRw_HL','MTc_HL','RTc_HL','ORDc_HL');
    
    for Nseq = 1:conf.Nseq
        
        str_LAB  = conf.(['LAB_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);   % trials for the given sequence: LAB scans
        str_HOME = conf.(['HOME_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);  % trials for the given sequence: HOME training
        
        % MT: take non-NaN elements from MT and select only wanted trials
        temp_MT = MT_HL{Nseq}(~isnan(MT_HL{Nseq}));
        temp_MT = temp_MT(1:(MaxSessionsLAB*str_LAB + MaxSessionsHOME*str_HOME));
        MT_HL_sbj{Nseq,i} = temp_MT; % assign back the temporary vector to the original vector
        
        % RT: take non-NaN elements from RT and select only wanted trials
        temp_RT = RT_HL{Nseq}(~isnan(RT_HL{Nseq}));
        temp_RT = temp_RT(1:(MaxSessionsLAB*str_LAB + MaxSessionsHOME*str_HOME));
        RT_HL_sbj{Nseq,i} = temp_RT; % assign back the temporary vector to the original vector
        
        % ERR: take non-NaN elements from ERR and select only wanted trials
        temp_ERR = ERR_HL{Nseq}(~isnan(ERR_HL{Nseq}));
        temp_ERR = temp_ERR(1:(MaxSessionsLAB*str_LAB + MaxSessionsHOME*str_HOME));
        ERR_HL_sbj{Nseq,i} = temp_ERR; % assign back the temporary vector to the original vector
        
        % ERRw: take non-NaN elements from ERRw and select only wanted trials
        temp_ERRw = ERRw_HL{Nseq}(~isnan(ERRw_HL{Nseq}));
        temp_ERRw = temp_ERRw(1:(MaxSessionsLAB*str_LAB + MaxSessionsHOME*str_HOME));
        ERRw_HL_sbj{Nseq,i} = temp_ERRw; % assign back the temporary vector to the original vector
        
    end
end


%% Average performance parameters (MT, RT, ERR and ERRw) in bins of size BINSIZE (computed for each participant), for EXT and MOD sequences
 
for i = 1:nSubjects
    
    %--------------------------------------------------------------------------
    %  1) MT
    % EXT: average EXT sequences and then average over BIN_SIZE
    MT_EXT = mean([MT_HL_sbj{1,i},MT_HL_sbj{2,i}],2);
    MT_EXT_BIN(:,i) = averageBIN(MT_EXT,BIN_SIZE);
    
    % MOD: average MOD sequences and then average over BIN_SIZE
    MT_MOD = mean([MT_HL_sbj{3,i},MT_HL_sbj{4,i}],2);
    MT_MOD_BIN(:,i) = averageBIN(MT_MOD,BIN_SIZE);
    
    %--------------------------------------------------------------------------
    %  2) ERR
    % EXT: average EXT sequences and then average over BIN_SIZE
    ERR_EXT_BIN(:,i) = mean([averageBIN(ERR_HL_sbj{1,i},BIN_SIZE),averageBIN(ERR_HL_sbj{2,i},BIN_SIZE)],2);
    
    % MOD: average EXT sequences and then average over BIN_SIZE
    ERR_MOD_BIN(:,i) = mean([averageBIN(ERR_HL_sbj{3,i},BIN_SIZE),averageBIN(ERR_HL_sbj{4,i},BIN_SIZE)],2);
    
    %--------------------------------------------------------------------------
    %  3) ERRw
    % EXT: average EXT sequences and then average over BIN_SIZE
    ERRw_EXT_BIN(:,i) = mean([averageBIN(ERRw_HL_sbj{1,i},BIN_SIZE),averageBIN(ERRw_HL_sbj{2,i},BIN_SIZE)],2);
    
    % MOD: average EXT sequences and then average over BIN_SIZE
    ERRw_MOD_BIN(:,i) = mean([averageBIN(ERRw_HL_sbj{3,i},BIN_SIZE),averageBIN(ERRw_HL_sbj{4,i},BIN_SIZE)],2);
    
    %--------------------------------------------------------------------------
    %  4) RT
    % EXT: average EXT sequences and then average over BIN_SIZE
    RT_EXT_BIN(:,i) = mean([averageBIN(RT_HL_sbj{1,i},BIN_SIZE),averageBIN(RT_HL_sbj{2,i},BIN_SIZE)],2);
    
    % MOD: average EXT sequences and then average over BIN_SIZE
    RT_MOD_BIN(:,i) = mean([averageBIN(RT_HL_sbj{3,i},BIN_SIZE),averageBIN(RT_HL_sbj{4,i},BIN_SIZE)],2);
    
end


%% rm ANOVA. Dependent variables: MT, RT, ERR and ERRw. -> Factors: PRACTICE (2 levels: EXT and MOD) and TIME_BINS (levels depend on the size of the vector)
 
min_BINS = min(size(MT_EXT_BIN,1),size(MT_MOD_BIN,1)); % number of time bins for statistical testing are computed based on the sequence that is practiced the less
 
%--------------------------------------------------------------------------
% 1) MT

% prepare data for anova table
temp_EXT = MT_EXT_BIN(1:min_BINS,:)';
temp_MOD = MT_MOD_BIN(1:min_BINS,:)';

% prepare varnames
F1 = {'EXT','MOD'}; % levels of factor 1
F2 = cell(1,min_BINS); % prepare levels of factor 2
for nlevF2 = 1:min_BINS
    F2{nlevF2} = ['TB',num2str(nlevF2)];
end

cont = 0;
for nlevF1 = 1:length(F1)
    for nlevF2 = 1:min_BINS
        
        cont = cont +1;
        varnames{1,cont} = [F1{nlevF1},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
        
    end
end

% prepare table with the data for the rm anova
t_MT = array2table([temp_EXT,temp_MOD],'VariableNames',varnames);

% this is the structure of the within subject factors
structure_practice = reshape(repmat((1:length(F1))',1,length(F2))',1,length(F1)*length(F2));
structure_time = repmat((1:length(F2)),1,length(F1));

WithinStructure_MT = table(categorical(structure_practice)',categorical(structure_time)','VariableNames',{'Practice','Time'});

rm_MT = fitrm(t_MT,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_MT,'WithinModel','Practice*Time');

ranovatable_MT = ranova(rm_MT,'WithinModel','Practice*Time');

clear temp_EXT temp_MOD


%--------------------------------------------------------------------------
% 2) ERR

% prepare data for anova table
temp_EXT = ERR_EXT_BIN(1:min_BINS,:)';
temp_MOD = ERR_MOD_BIN(1:min_BINS,:)';

% prepare varnames
F1 = {'EXT','MOD'}; % levels of factor 1
F2 = cell(1,min_BINS); % prepare levels of factor 2
for nlevF2 = 1:min_BINS
    F2{nlevF2} = ['TB',num2str(nlevF2)];
end

cont = 0;
for nlevF1 = 1:length(F1)
    for nlevF2 = 1:min_BINS
        
        cont = cont +1;
        varnames{1,cont} = [F1{nlevF1},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
        
    end
end

% prepare table with the data for the rm anova
t_ERR = array2table([temp_EXT,temp_MOD],'VariableNames',varnames);

% this is the structure of the within subject factors
structure_practice = reshape(repmat((1:length(F1))',1,length(F2))',1,length(F1)*length(F2));
structure_time = repmat((1:length(F2)),1,length(F1));

WithinStructure_ERR = table(categorical(structure_practice)',categorical(structure_time)','VariableNames',{'Practice','Time'});

rm_ERR = fitrm(t_ERR,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_ERR,'WithinModel','Practice*Time');

ranovatable_ERR = ranova(rm_ERR,'WithinModel','Practice*Time');

clear temp_EXT temp_MOD

%--------------------------------------------------------------------------
% 3) ERRw

% prepare data for anova table
temp_EXT = ERRw_EXT_BIN(1:min_BINS,:)';
temp_MOD = ERRw_MOD_BIN(1:min_BINS,:)';

% prepare varnames
F1 = {'EXT','MOD'}; % levels of factor 1
F2 = cell(1,min_BINS); % prepare levels of factor 2
for nlevF2 = 1:min_BINS
    F2{nlevF2} = ['TB',num2str(nlevF2)];
end

cont = 0;
for nlevF1 = 1:length(F1)
    for nlevF2 = 1:min_BINS
        
        cont = cont +1;
        varnames{1,cont} = [F1{nlevF1},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
        
    end
end

% prepare table with the data for the rm anova
t_ERRw = array2table([temp_EXT,temp_MOD],'VariableNames',varnames);

% this is the structure of the within subject factors
structure_practice = reshape(repmat((1:length(F1))',1,length(F2))',1,length(F1)*length(F2));
structure_time = repmat((1:length(F2)),1,length(F1));

WithinStructure_ERRw = table(categorical(structure_practice)',categorical(structure_time)','VariableNames',{'Practice','Time'});

rm_ERRw = fitrm(t_ERRw,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_ERRw,'WithinModel','Practice*Time');

ranovatable_ERRw = ranova(rm_ERRw,'WithinModel','Practice*Time');

clear temp_EXT temp_MOD

%--------------------------------------------------------------------------
% 4) RT

% prepare data for anova table
temp_EXT = RT_EXT_BIN(1:min_BINS,:)';
temp_MOD = RT_MOD_BIN(1:min_BINS,:)';

% prepare varnames
F1 = {'EXT','MOD'}; % levels of factor 1
F2 = cell(1,min_BINS); % prepare levels of factor 2
for nlevF2 = 1:min_BINS
    F2{nlevF2} = ['TB',num2str(nlevF2)];
end

cont = 0;
for nlevF1 = 1:length(F1)
    for nlevF2 = 1:min_BINS
        
        cont = cont +1;
        varnames{1,cont} = [F1{nlevF1},'_',F2{nlevF2}]; %varname containing name of the variables for the anova table
        
    end
end

% prepare table with the data for the rm anova
t_RT = array2table([temp_EXT,temp_MOD],'VariableNames',varnames);

% this is the structure of the within subject factors
structure_practice = reshape(repmat((1:length(F1))',1,length(F2))',1,length(F1)*length(F2));
structure_time = repmat((1:length(F2)),1,length(F1));

WithinStructure_RT = table(categorical(structure_practice)',categorical(structure_time)','VariableNames',{'Practice','Time'});

rm_RT = fitrm(t_RT,[varnames{1},'-',varnames{end},' ~ 1'],'WithinDesign',WithinStructure_RT,'WithinModel','Practice*Time');

ranovatable_RT = ranova(rm_RT,'WithinModel','Practice*Time');

clear temp_EXT temp_MOD


%% Plots
%--------------------------------------------------------------------------
% 1) Prepare MT for errorbar plot

%    EXT
MT_EXT_BIN_mean = mean(MT_EXT_BIN(1:min_BINS,:),2); % mean over participants
MT_EXT_BIN_sem  = std(MT_EXT_BIN(1:min_BINS,:),0,2)/sqrt(nSubjects); % sem of the mean

%    MOD
MT_MOD_BIN_mean = mean(MT_MOD_BIN(1:min_BINS,:),2); % mean over participants
MT_MOD_BIN_sem  = std(MT_MOD_BIN(1:min_BINS,:),0,2)/sqrt(nSubjects); % sem of the mean

% PLOT
fig_num = 1; 
txt_title = ['LAB (sessions = ',num2str(MaxSessionsLAB),') and HOME (sessions = ',...
              num2str(MaxSessionsHOME),') trainings; Subj = ',num2str(nSubjects),...
             '; BIN size = ',num2str(BIN_SIZE),' trials'];
         
axis_values = [0 min_BINS+1 0 6];
txt_legend = {'EXT','MOD'};
txt_axisx = 'Trials';
txt_axisy = 'Movement Time [sec]';
plot_MT_RT_ERR_LAB_HOME(MT_EXT_BIN_mean,MT_MOD_BIN_mean,[],...
                        MT_EXT_BIN_sem,MT_MOD_BIN_sem,[],...
                        min_BINS,BIN_SIZE,axis_values,txt_title,txt_axisx,txt_axisy,fig_num,txt_legend)


%--------------------------------------------------------------------------
% 2) Prepare ERR for errorbar plot

%    EXT
ERR_EXT_BIN_mean = mean(ERR_EXT_BIN(1:min_BINS,:),2); % mean over participants
ERR_EXT_BIN_sem  = std(ERR_EXT_BIN(1:min_BINS,:),0,2)/sqrt(nSubjects); % sem of the mean

%    MOD
ERR_MOD_BIN_mean = mean(ERR_MOD_BIN(1:min_BINS,:),2); % mean over participants
ERR_MOD_BIN_sem  = std(ERR_MOD_BIN(1:min_BINS,:),0,2)/sqrt(nSubjects); % sem of the mean

% PLOT
fig_num = 2; 
txt_title = ['LAB (sessions = ',num2str(MaxSessionsLAB),') and HOME (sessions = ',...
              num2str(MaxSessionsHOME),') trainings; Subj = ',num2str(nSubjects),...
             '; BIN size = ',num2str(BIN_SIZE),' trials'];
         
axis_values = [0 min_BINS+1 0 1];
txt_legend = {'EXT','MOD'};
txt_axisx = 'Trials';
txt_axisy = 'Error Rate';
plot_MT_RT_ERR_LAB_HOME(ERR_EXT_BIN_mean,ERR_MOD_BIN_mean,[],...
                        ERR_EXT_BIN_sem,ERR_MOD_BIN_sem,[],...
                        min_BINS,BIN_SIZE,axis_values,txt_title,txt_axisx,txt_axisy,fig_num,txt_legend)

%--------------------------------------------------------------------------
% 3) Prepare ERRw for errorbar plot

%    EXT
ERRw_EXT_BIN_mean = mean(ERRw_EXT_BIN(1:min_BINS,:),2); % mean over participants
ERRw_EXT_BIN_sem  = std(ERRw_EXT_BIN(1:min_BINS,:),0,2)/sqrt(nSubjects); % sem of the mean

%    MOD
ERRw_MOD_BIN_mean = mean(ERRw_MOD_BIN(1:min_BINS,:),2); % mean over participants
ERRw_MOD_BIN_sem  = std(ERRw_MOD_BIN(1:min_BINS,:),0,2)/sqrt(nSubjects); % sem of the mean

% PLOT
fig_num = 3; 
txt_title = ['LAB (sessions = ',num2str(MaxSessionsLAB),') and HOME (sessions = ',...
              num2str(MaxSessionsHOME),') trainings; Subj = ',num2str(nSubjects),...
             '; BIN size = ',num2str(BIN_SIZE),' trials'];
         
axis_values = [0 min_BINS+1 0 0.5];
txt_legend = {'EXT','MOD'};
txt_axisx = 'Trials';
txt_axisy = 'Error Rate Weighted';
plot_MT_RT_ERR_LAB_HOME(ERRw_EXT_BIN_mean,ERRw_MOD_BIN_mean,[],...
                        ERRw_EXT_BIN_sem,ERRw_MOD_BIN_sem,[],...
                        min_BINS,BIN_SIZE,axis_values,txt_title,txt_axisx,txt_axisy,fig_num,txt_legend)
                    

%--------------------------------------------------------------------------
% 4) Prepare RT for errorbar plot

%    EXT
RT_EXT_BIN_mean = mean(RT_EXT_BIN(1:min_BINS,:),2); % mean over participants
RT_EXT_BIN_sem  = std(RT_EXT_BIN(1:min_BINS,:),0,2)/sqrt(nSubjects); % sem of the mean

%    MOD
RT_MOD_BIN_mean = mean(RT_MOD_BIN(1:min_BINS,:),2); % mean over participants
RT_MOD_BIN_sem  = std(RT_MOD_BIN(1:min_BINS,:),0,2)/sqrt(nSubjects); % sem of the mean

% PLOT
fig_num = 4; 
txt_title = ['LAB (sessions = ',num2str(MaxSessionsLAB),') and HOME (sessions = ',...
              num2str(MaxSessionsHOME),') trainings; Subj = ',num2str(nSubjects),...
             '; BIN size = ',num2str(BIN_SIZE),' trials'];
         
axis_values = [0 min_BINS+1 0 1];
txt_legend = {'EXT','MOD'};
txt_axisx = 'Trials';
txt_axisy = 'Response Time [sec]';
plot_MT_RT_ERR_LAB_HOME(RT_EXT_BIN_mean,RT_MOD_BIN_mean,[],...
                        RT_EXT_BIN_sem,RT_MOD_BIN_sem,[],...
                        min_BINS,BIN_SIZE,axis_values,txt_title,txt_axisx,txt_axisy,fig_num,txt_legend)

                    
%% save figures 

if saveflag 
    saveas(figure(1),[conf.PLOT_FOLDER_PRAC_INT filesep 'GrandAverage_MovTime_binsize_',num2str(BIN_SIZE),'.png'])
    saveas(figure(4),[conf.PLOT_FOLDER_PRAC_INT filesep 'GrandAverage_ResTime_binsize_',num2str(BIN_SIZE),'.png'])
    saveas(figure(2),[conf.PLOT_FOLDER_PRAC_INT filesep 'GrandAverage_ErrRate_binsize_',num2str(BIN_SIZE),'.png'])
    saveas(figure(3),[conf.PLOT_FOLDER_PRAC_INT filesep 'GrandAverage_ErrRateWeighted_binsize_',num2str(BIN_SIZE),'.png'])
end
            


