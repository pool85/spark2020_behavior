% DataExctraction_HOME.m 
%
% SPARK PROJECT 2020. DSP task - Home-based Sessions
% This script extract the behavioural data from the participants (contained
% in the .csv files in ../raw/home) and save them into a matlab structure
% (in ../extracted/home)
%
% Last mod: 30.06.2020 (changed way to read CSV tables) by PR
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)
clc
clear all

%% Initialize the script
addpath('..');
initAnalysis

%% Find file names and subject IDs
[csvFileNames, subjects] = getSubjectIds(conf.RAW_FOLDER_HOME);

nSubjects = length(subjects); % number of subjects


%% Data extraction
for i = 1:nSubjects 

    SubjectsId = subjects{i};
    
    Sessions = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions = length(Sessions);
    
    disp(['From the number of raw files, it seems that subject ', SubjectsId,' has performed ',num2str(nSessions),' sessions.'])
    
    outputDataset = [ conf.EXTRACTED_FOLDER_HOME filesep SubjectsId filesep SubjectsId '.mat'];
    
    
    for j = 1:nSessions
        
        SessionsId = Sessions{j};
        
        inputDataset  = [ conf.RAW_FOLDER_HOME filesep SessionsId '.csv'];
        
        if ~exist(inputDataset, 'file')
            error('Output from psychopy does not exist');
        end
        
        
        % Create folder if does not exist
        createFolderPath(outputDataset);
        
        % 1: Load the variables of interest from psychopy output
        table = readtable(inputDataset);      
        
        sessionCSV = sprintf('Session_%03d',table.session(1));
       
        % Correctness of responses to the display: correct (1) or uncorrect (0)
        Data.(sessionCSV).dispError = table.key_resp_2_corr(~isnan(table.key_resp_2_corr));
        
        % Response Time (RT)to displays (correct and uncorrect)
        Data.(sessionCSV).dispRT = table.key_resp_2_rt(~isnan(table.key_resp_2_rt));
        
        % Information whether the display is new (0) or repeated (1)
        Data.(sessionCSV).dispRep = table.trials_thisRepN(~isnan(table.trials_thisRepN));
        
        % Number of errors for each display
        Data.(sessionCSV).numDispError = table.num_errors(~isnan(table.num_errors));
        
        
        % Both measure of MT below account for the time elapsed from
        %       the presentation of the stimulus to the last button press. To obtain
        %       the "real MT", the RT to the first display of the sequence should
        %       be substracted from the corresponing MT vectors)! 
        
        % Movement Time (MT) to sequences. This MT is made up of first RT responses to display stimuli (whether they are correct or not).   
        Data.(sessionCSV).seqDuration = table.Sequence_Duration(~isnan(table.Sequence_Duration));
        
        % Movement Time (MT) to sequences. This MT is made up of all the responses needed to complete the sequence (accounting for the RT of the repetitions).  
        Data.(sessionCSV).seqDurationAll = table.Sequence_Duration_all(~isnan(table.Sequence_Duration_all));
        
        % Number of errors for each sequence
        Data.(sessionCSV).seqErrors = table.num_errors_sequence(~isnan(table.num_errors_sequence));
        
        % Type of practiced sequence (EXT1, EXT2, etc .. ) 
        temp = {};
        for n = 1:length(table.condsFile) 
            if contains(table.condsFile{n},conf.seq1) == 1
                temp = [temp; conf.seq1];
            elseif contains(table.condsFile{n},conf.seq2) == 1
                temp = [temp; conf.seq2];
            elseif contains(table.condsFile{n},conf.seq3) == 1
                temp = [temp; conf.seq3];
            elseif contains(table.condsFile{n},conf.seq4) == 1
                temp = [temp; conf.seq4];
            elseif contains(table.condsFile{n},conf.seq5) == 1
                temp = [temp; conf.seq5];
            elseif contains(table.condsFile{n},conf.seq6) == 1
                temp = [temp; conf.seq6];
            end
        end
        
        Data.(sessionCSV).seqType = temp;
         
    end
    
    save(outputDataset,'Data')
    clear Data
     
end