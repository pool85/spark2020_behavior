% DataElaboration_LAB.m
%
% SPARK PROJECT 2020.
%
% Get Movement Time (MT), Error rates (ERR: computed as the number of wrong sequence over the total sequences), Error rates weighted (ERRw: computed taking into account the number of errors / sequence),  and Response Time (RT)
% from laboratory sessions. The data are organized according to participants, sessions, and epochs, in tables.
%
% The script also outputs che vectors of MT for entirely correct sequences.
%
% In addition, the raw data are also exctracted in corresponding tables.
% This will be useful if any additional analysis is envisioned by putting
% together LAB and HOME-based training sessions
%
% Modified the 09.11.2020 to process subjects with missing sessions (e.g. participant SGOHF) in
% between the 4 scans, and to correct the computation of ERRw
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)
clc
clear all

%% Initialize the script
addpath('..');
initAnalysis

%% Find file names and subject IDs
[csvFileNames, subjects] = getSubjectIds(conf.RAW_FOLDER_LAB);

nSubjects = length(subjects); % number of subjects


%% For each subject, get data from each scan session

for i = 1:nSubjects
    
    SubjectsId = subjects{i};
    
    inputDataset  = [ conf.EXTRACTED_FOLDER_LAB filesep SubjectsId filesep SubjectsId '.mat'];
    outputDataset = [ conf.EXTRACTED_FOLDER_LAB filesep SubjectsId filesep SubjectsId '_elaborated.mat'];
    
    load(inputDataset,'Data');
    
    nSessions = numel(fieldnames(Data));
    SessionNames = fieldnames(Data);
    
    %% check if number of sessions is lower than max session number (added to include missing sessions- e.g. participant SGOHF)
    temp = [];
    for loop = 1:length(SessionNames)
        temp(loop) = str2double(SessionNames{loop}(end));
    end
    temp = max(temp);
    if temp > nSessions
        MT = NaN*ones(10,conf.LAB_Epochs,conf.Nseq,temp);
        RT = NaN*ones(10,conf.LAB_Epochs,conf.Nseq,temp);
        ERR = NaN*ones(10,conf.LAB_Epochs,conf.Nseq,temp);
        ERRw = NaN*ones(10,conf.LAB_Epochs,conf.Nseq,temp);
        
    end
    
    clear temp
    
    
    
    
    %% Exctract subject's data
    for nsess = 1:nSessions
        session_str = SessionNames{nsess};
        sessionNum = str2double(session_str(end));
        
        for nepochs = 1:conf.LAB_Epochs
            epoch_str = ['E_',num2str(nepochs)];
            
            % 1) MT: Movement time. seqDuration is the sum of the first responses to DSP displays (whther they are correct or not).
            %     If you want the global duration (i.e., the sum to all displays, select seqDurationAll). To get the real MT, you
            %     will have to substract the RT to the first DSP display in each trial
            MT(:,nepochs,1,sessionNum) = Data.(session_str).(epoch_str).seqDuration(contains(Data.(session_str).(epoch_str).seqType,conf.seq1));
            MT(:,nepochs,2,sessionNum) = Data.(session_str).(epoch_str).seqDuration(contains(Data.(session_str).(epoch_str).seqType,conf.seq2));
            MT(:,nepochs,3,sessionNum) = Data.(session_str).(epoch_str).seqDuration(contains(Data.(session_str).(epoch_str).seqType,conf.seq3));
            MT(:,nepochs,4,sessionNum) = Data.(session_str).(epoch_str).seqDuration(contains(Data.(session_str).(epoch_str).seqType,conf.seq4));
            MT(:,nepochs,5,sessionNum) = Data.(session_str).(epoch_str).seqDuration(contains(Data.(session_str).(epoch_str).seqType,conf.seq5));
            MT(:,nepochs,6,sessionNum) = Data.(session_str).(epoch_str).seqDuration(contains(Data.(session_str).(epoch_str).seqType,conf.seq6));
            
            
            % 2) MT of correct trials only (seqErrors is the number of
            %                               total errors committed during
            %                               sequence execution)
            MTcorr{nepochs,1,sessionNum} = Data.(session_str).(epoch_str).seqDuration(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq1),Data.(session_str).(epoch_str).seqErrors == 0));
            MTcorr{nepochs,2,sessionNum} = Data.(session_str).(epoch_str).seqDuration(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq2),Data.(session_str).(epoch_str).seqErrors == 0));
            MTcorr{nepochs,3,sessionNum} = Data.(session_str).(epoch_str).seqDuration(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq3),Data.(session_str).(epoch_str).seqErrors == 0));
            MTcorr{nepochs,4,sessionNum} = Data.(session_str).(epoch_str).seqDuration(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq4),Data.(session_str).(epoch_str).seqErrors == 0));
            MTcorr{nepochs,5,sessionNum} = Data.(session_str).(epoch_str).seqDuration(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq5),Data.(session_str).(epoch_str).seqErrors == 0));
            MTcorr{nepochs,6,sessionNum} = Data.(session_str).(epoch_str).seqDuration(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq6),Data.(session_str).(epoch_str).seqErrors == 0));
            
            
            % Index of correct trials (trials for each sequence type are ordered sequentially in each epoch, and then only the
            %          correct trials are retained). This output needs to be associated to MTcorr and RTcorr (see below)
            vec_seq_order = seqOrder_LAB(60,10,Data.(session_str).(epoch_str).seqType,conf.seq1,conf.seq2,conf.seq3,conf.seq4,conf.seq5,conf.seq6);
            ORDERcorr{nepochs,1,sessionNum} = vec_seq_order(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq1),Data.(session_str).(epoch_str).seqErrors == 0));
            ORDERcorr{nepochs,2,sessionNum} = vec_seq_order(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq2),Data.(session_str).(epoch_str).seqErrors == 0));
            ORDERcorr{nepochs,3,sessionNum} = vec_seq_order(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq3),Data.(session_str).(epoch_str).seqErrors == 0));
            ORDERcorr{nepochs,4,sessionNum} = vec_seq_order(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq4),Data.(session_str).(epoch_str).seqErrors == 0));
            ORDERcorr{nepochs,5,sessionNum} = vec_seq_order(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq5),Data.(session_str).(epoch_str).seqErrors == 0));
            ORDERcorr{nepochs,6,sessionNum} = vec_seq_order(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq6),Data.(session_str).(epoch_str).seqErrors == 0));
            
            
            % RT: Response time to the first stimulus display
            temp = Data.(session_str).(epoch_str).RT(1:10:end); % take first RT to DSP stimuli of each trial
            
            RT(:,nepochs,1,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq1));
            RT(:,nepochs,2,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq2));
            RT(:,nepochs,3,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq3));
            RT(:,nepochs,4,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq4));
            RT(:,nepochs,5,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq5));
            RT(:,nepochs,6,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq6));
            
            
            % RT of correct trials only
            temp = Data.(session_str).(epoch_str).RT(1:10:end); % take first RT to DSP stimuli of each trial
            
            RTcorr{nepochs,1,sessionNum} = temp(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq1),Data.(session_str).(epoch_str).seqErrors == 0));
            RTcorr{nepochs,2,sessionNum} = temp(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq2),Data.(session_str).(epoch_str).seqErrors == 0));
            RTcorr{nepochs,3,sessionNum} = temp(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq3),Data.(session_str).(epoch_str).seqErrors == 0));
            RTcorr{nepochs,4,sessionNum} = temp(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq4),Data.(session_str).(epoch_str).seqErrors == 0));
            RTcorr{nepochs,5,sessionNum} = temp(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq5),Data.(session_str).(epoch_str).seqErrors == 0));
            RTcorr{nepochs,6,sessionNum} = temp(and(contains(Data.(session_str).(epoch_str).seqType,conf.seq6),Data.(session_str).(epoch_str).seqErrors == 0));
            
            
            % ERR: Errors committed during the execution of the sequence
            ERR(:,nepochs,1,sessionNum) = Data.(session_str).(epoch_str).seqErrors(contains(Data.(session_str).(epoch_str).seqType,conf.seq1));
            ERR(:,nepochs,2,sessionNum) = Data.(session_str).(epoch_str).seqErrors(contains(Data.(session_str).(epoch_str).seqType,conf.seq2));
            ERR(:,nepochs,3,sessionNum) = Data.(session_str).(epoch_str).seqErrors(contains(Data.(session_str).(epoch_str).seqType,conf.seq3));
            ERR(:,nepochs,4,sessionNum) = Data.(session_str).(epoch_str).seqErrors(contains(Data.(session_str).(epoch_str).seqType,conf.seq4));
            ERR(:,nepochs,5,sessionNum) = Data.(session_str).(epoch_str).seqErrors(contains(Data.(session_str).(epoch_str).seqType,conf.seq5));
            ERR(:,nepochs,6,sessionNum) = Data.(session_str).(epoch_str).seqErrors(contains(Data.(session_str).(epoch_str).seqType,conf.seq6));
            
            % ERRw: Proportion of wrong displays/sequence (take numDispError, assign it to 1 even if more mistakes are done for a particular display, average each 10 displays of the 60 sequences to get the accuracy)
            temp = Data.(session_str).(epoch_str).dispErrors; temp(temp>0) = 1; temp = mean(reshape(temp',10,60),1)';    % 10 is the n� of displays and 60 the number of trials(sequences) in each epoch of the lab scan
            ERRw(:,nepochs,1,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq1));
            ERRw(:,nepochs,2,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq2));
            ERRw(:,nepochs,3,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq3));
            ERRw(:,nepochs,4,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq4));
            ERRw(:,nepochs,5,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq5));
            ERRw(:,nepochs,6,sessionNum) = temp(contains(Data.(session_str).(epoch_str).seqType,conf.seq6));
            
        end
        
    end
       
    % ERRt: total number of uncorrect responses during sequence execution. Many mistakes can be done on the same display! 
    ERRt = ERR;
    
    % Modify ERR such that the sequence is identified as entirely correct (0) or with at least one error (1)
    ERR((ERR>0)) = 1;
    
    % Modify MT such that MT is the time between first and last button press (i.e.: remove RT to first DSP display from MT)
    MT = MT - RT;
    
    % Modify MTcorr such that MTcorr is the time between first and last button press (i.e.: remove RTcorr to first DSP display from MT)
    for nseq = 1:conf.Nseq
        for nep = 1:nepochs
            for nsess = 1:size(MT,4)
                MTcorr{nep,nseq,nsess} = MTcorr{nep,nseq,nsess} - RTcorr{nep,nseq,nsess};
            end
        end
    end
    
    
    %% MT processing
    
    % check for outliers in MT (values > 20 seconds)
    temp = MT;
    ind_out = find((MT>20));
    temp(ind_out) = [];
    MT(ind_out) = ones(length(ind_out),1)* mean(temp(:));
    
    if isempty(ind_out)
        disp([SubjectsId,': NO outliers in MT'])
    else
        disp([SubjectsId,': Found ',num2str(length(ind_out)),' outliers in MT'])
    end
    
    
    % check for outliers in RT (values > 4 seconds)
    temp = RT;
    ind_out = find((RT>3));
    temp(ind_out) = [];
    RT(ind_out) = ones(length(ind_out),1)* mean(temp(:));
    
    if isempty(ind_out)
        disp([SubjectsId,': NO outliers in RT'])
    else
        disp([SubjectsId,': Found ',num2str(length(ind_out)),' outliers in RT'])
    end
    
    
    % check for outliers in MTcorr (values > 20 seconds) and replace by the mean value around it
    cont = 0;
    temp = MTcorr;
    for nseq = 1:conf.Nseq
        for nep = 1:nepochs
            for nsess = 1:size(MT,4)
                
                ind_out = find((MTcorr{nep,nseq,nsess} > 20));   cont = cont + length(ind_out);
                
                if ~isempty(temp{nep,nseq,nsess})
                    temp{nep,nseq,nsess}(ind_out) = [];
                    MTcorr{nep,nseq,nsess}(ind_out) = ones(length(ind_out),1) * mean(temp{nep,nseq,nsess});
                end
                
            end
        end
    end
    
    if cont == 0
        disp([SubjectsId,': NO outliers in MTcorr'])
    else
        disp([SubjectsId,': Found ',num2str(cont),' outliers in MTcorr'])
    end
    
    
    % check for outliers in RTcorr (values > 4 seconds) and replace by the mean value around it
    cont = 0;
    temp = RTcorr;
    for nseq = 1:conf.Nseq
        for nep = 1:nepochs
            for nsess = 1:size(MT,4)
                
                ind_out = find((RTcorr{nep,nseq,nsess} > 4));   cont = cont + length(ind_out);
                
                if ~isempty(temp{nep,nseq,nsess})
                    temp{nep,nseq,nsess}(ind_out) = [];
                    RTcorr{nep,nseq,nsess}(ind_out) = ones(length(ind_out),1) * mean(temp{nep,nseq,nsess});
                end
                
            end
        end
    end
    
    if cont == 0
        disp([SubjectsId,': NO outliers in RTcorr'])
    else
        disp([SubjectsId,': Found ',num2str(cont),' outliers in RTcorr'])
    end
    
    
    % Data Processing: EXT
    MT_EXT = squeeze(mean(MT(:,:,1:2,:),3));  % Average over sequences of the same type (EXT); DIM = 3 if nsessions > 1, otherwise DIM = 2
    MT_EXT_MEAN =  squeeze(mean(MT_EXT,1));   % Mean MT, for each epoch and session
    MT_EXT_SEM =  squeeze(std(MT_EXT,0,1)/sqrt(size(MT_EXT,1))); % compute Standard error of the mean
    
    % Data Processing: MOD
    MT_MOD = squeeze(mean(MT(:,:,3:4,:),3));  % Average over sequences ofthe same type (MOD); DIM = 3 if nsessions > 1, otherwise DIM = 2
    MT_MOD_MEAN =  squeeze(mean(MT_MOD,1));   % Mean MT, for each epoch and session
    MT_MOD_SEM =  squeeze(std(MT_MOD,0,1)/sqrt(size(MT_MOD,1)));
    
    % Data Processing: MIN
    MT_MIN = squeeze(mean(MT(:,:,5:6,:),3));  % Average over sequences ofthe same type (MIN); DIM = 3 if nsessions > 1, otherwise DIM = 2
    MT_MIN_MEAN =  squeeze(mean(MT_MIN,1));   % Mean MT, for each epoch and session
    MT_MIN_SEM =  squeeze(std(MT_MIN,0,1)/sqrt(size(MT_MIN,1)));
    
    if length(size(MT)) < 4  % if there is only one session
        
        MT_EXT_MEAN = MT_EXT_MEAN';
        MT_EXT_SEM = MT_EXT_SEM';
        MT_MOD_MEAN = MT_MOD_MEAN';
        MT_MOD_SEM = MT_MOD_SEM';
        MT_MIN_MEAN = MT_MIN_MEAN';
        MT_MIN_SEM = MT_MIN_SEM';
        
    end
    
    % Create table for MT
    Scan_session = reshape(repmat(1:size(MT,4),5,1),size(MT,4)*conf.LAB_Epochs,1);
    Session_epoch = repmat(1:conf.LAB_Epochs,1,size(MT,4))';
    MT_EXT_MEAN = reshape(MT_EXT_MEAN,size(MT,4)*conf.LAB_Epochs,1);
    MT_MOD_MEAN = reshape(MT_MOD_MEAN,size(MT,4)*conf.LAB_Epochs,1);
    MT_MIN_MEAN = reshape(MT_MIN_MEAN,size(MT,4)*conf.LAB_Epochs,1);
    MT_EXT_SEM = reshape(MT_EXT_SEM,size(MT,4)*conf.LAB_Epochs,1);
    MT_MOD_SEM = reshape(MT_MOD_SEM,size(MT,4)*conf.LAB_Epochs,1);
    MT_MIN_SEM = reshape(MT_MIN_SEM,size(MT,4)*conf.LAB_Epochs,1);
    
    MT_Table = table(Scan_session,Session_epoch,MT_EXT_MEAN,MT_MOD_MEAN,MT_MIN_MEAN,MT_EXT_SEM,MT_MOD_SEM,MT_MIN_SEM);
    
    
    %% RT processing
    
    % Data Processing: EXT
    RT_EXT = squeeze(mean(RT(:,:,1:2,:),3));  % Average over sequences of the same type (EXT); DIM = 3 if nsessions > 1, otherwise DIM = 2
    RT_EXT_MEAN =  squeeze(mean(RT_EXT,1));   % Mean MT, for each epoch and session
    RT_EXT_SEM =  squeeze(std(RT_EXT,0,1)/sqrt(size(RT_EXT,1)));
    
    % Data Processing: MOD
    RT_MOD = squeeze(mean(RT(:,:,3:4,:),3));  % Average over sequences ofthe same type (MOD); DIM = 3 if nsessions > 1, otherwise DIM = 2
    RT_MOD_MEAN =  squeeze(mean(RT_MOD,1));   % Mean MT, for each epoch and session
    RT_MOD_SEM =  squeeze(std(RT_MOD,0,1)/sqrt(size(RT_MOD,1)));
    
    % Data Processing: MIN
    RT_MIN = squeeze(mean(RT(:,:,5:6,:),3));  % Average over sequences ofthe same type (MIN); DIM = 3 if nsessions > 1, otherwise DIM = 2
    RT_MIN_MEAN =  squeeze(mean(RT_MIN,1));   % Mean MT, for each epoch and session
    RT_MIN_SEM =  squeeze(std(RT_MIN,0,1)/sqrt(size(RT_MIN,1)));
    
    if length(size(RT)) < 4  % if there is only one session
        
        RT_EXT_MEAN = RT_EXT_MEAN';
        RT_EXT_SEM = RT_EXT_SEM';
        RT_MOD_MEAN = RT_MOD_MEAN';
        RT_MOD_SEM = RT_MOD_SEM';
        RT_MIN_MEAN = RT_MIN_MEAN';
        RT_MIN_SEM = RT_MIN_SEM';
        
    end
    
    % Create table for RT
    Scan_session = reshape(repmat(1:size(RT,4),5,1),size(RT,4)*conf.LAB_Epochs,1);
    Session_epoch = repmat(1:conf.LAB_Epochs,1,size(RT,4))';
    RT_EXT_MEAN = reshape(RT_EXT_MEAN,size(RT,4)*conf.LAB_Epochs,1);
    RT_MOD_MEAN = reshape(RT_MOD_MEAN,size(RT,4)*conf.LAB_Epochs,1);
    RT_MIN_MEAN = reshape(RT_MIN_MEAN,size(RT,4)*conf.LAB_Epochs,1);
    RT_EXT_SEM = reshape(RT_EXT_SEM,size(RT,4)*conf.LAB_Epochs,1);
    RT_MOD_SEM = reshape(RT_MOD_SEM,size(RT,4)*conf.LAB_Epochs,1);
    RT_MIN_SEM = reshape(RT_MIN_SEM,size(RT,4)*conf.LAB_Epochs,1);
    
    RT_Table = table(Scan_session,Session_epoch,RT_EXT_MEAN,RT_MOD_MEAN,RT_MIN_MEAN,RT_EXT_SEM,RT_MOD_SEM,RT_MIN_SEM);
    
    
    %% ERR processing
    
    % Data Processing: EXT
    ERR_EXT = ERR(:,:,1:2,:);
    ERR_EXT_MEAN = squeeze(mean(mean(mean(ERR_EXT,1),2),3));   %  vector with number of elements equal the number of scan sessions.
    ERR_EXT_STD = std(reshape(ERR_EXT,size(ERR_EXT,1)*size(ERR_EXT,2)*size(ERR_EXT,3),size(ERR,4)),0,1);
    ERR_EXT_SEM = ERR_EXT_STD/sqrt(size(ERR_EXT,1)*size(ERR_EXT,2)*size(ERR_EXT,3));
    
    % Data Processing: MOD
    ERR_MOD = ERR(:,:,3:4,:);
    ERR_MOD_MEAN = squeeze(mean(mean(mean(ERR_MOD,1),2),3));   %  vector with number of elements equal the number of scan sessions.
    ERR_MOD_STD = std(reshape(ERR_MOD,size(ERR_MOD,1)*size(ERR_MOD,2)*size(ERR_MOD,3),size(ERR,4)),0,1);
    ERR_MOD_SEM = ERR_MOD_STD/sqrt(size(ERR_MOD,1)*size(ERR_MOD,2)*size(ERR_MOD,3));
    
    % Data Processing: MIN
    ERR_MIN = ERR(:,:,5:6,:);
    ERR_MIN_MEAN = squeeze(mean(mean(mean(ERR_MIN,1),2),3));   %  vector with number of elements equal the number of scan sessions.
    ERR_MIN_STD = std(reshape(ERR_MIN,size(ERR_MIN,1)*size(ERR_MIN,2)*size(ERR_MIN,3),size(ERR,4)),0,1);
    ERR_MIN_SEM = ERR_MIN_STD/sqrt(size(ERR_MIN,1)*size(ERR_MIN,2)*size(ERR_MIN,3));
    
    % Create table for ERR
    Scan_session = (1:size(ERR,4))';
    ERR_EXT_SEM = ERR_EXT_SEM';
    ERR_MOD_SEM = ERR_MOD_SEM';
    ERR_MIN_SEM = ERR_MIN_SEM';
    
    ERR_Table = table(Scan_session,ERR_EXT_MEAN,ERR_MOD_MEAN,ERR_MIN_MEAN,ERR_EXT_SEM,ERR_MOD_SEM,ERR_MIN_SEM);
    
    
    %% ERRw processing
    
    % Data Processing: EXT
    ERRw_EXT = ERRw(:,:,1:2,:);
    ERRw_EXT_MEAN = squeeze(mean(mean(mean(ERRw_EXT,1),2),3));   %  vector with number of elements equal the number of scan sessions.
    ERRw_EXT_STD = std(reshape(ERRw_EXT,size(ERRw_EXT,1)*size(ERRw_EXT,2)*size(ERRw_EXT,3),size(ERR,4)),0,1);
    ERRw_EXT_SEM = ERRw_EXT_STD/sqrt(size(ERRw_EXT,1)*size(ERRw_EXT,2)*size(ERRw_EXT,3));
    
    % Data Processing: MOD
    ERRw_MOD = ERRw(:,:,3:4,:);
    ERRw_MOD_MEAN = squeeze(mean(mean(mean(ERRw_MOD,1),2),3));   %  vector with number of elements equal the number of scan sessions.
    ERRw_MOD_STD = std(reshape(ERRw_MOD,size(ERRw_MOD,1)*size(ERRw_MOD,2)*size(ERRw_MOD,3),size(ERR,4)),0,1);
    ERRw_MOD_SEM = ERRw_MOD_STD/sqrt(size(ERRw_MOD,1)*size(ERRw_MOD,2)*size(ERRw_MOD,3));
    
    % Data Processing: MIN
    ERRw_MIN = ERRw(:,:,5:6,:);
    ERRw_MIN_MEAN = squeeze(mean(mean(mean(ERRw_MIN,1),2),3));   %  vector with number of elements equal the number of scan sessions.
    ERRw_MIN_STD = std(reshape(ERRw_MIN,size(ERRw_MIN,1)*size(ERRw_MIN,2)*size(ERRw_MIN,3),size(ERR,4)),0,1);
    ERRw_MIN_SEM = ERRw_MIN_STD/sqrt(size(ERRw_MIN,1)*size(ERRw_MIN,2)*size(ERRw_MIN,3));
    
    % Create table for ERRw
    Scan_session = (1:size(ERR,4))';
    ERRw_EXT_SEM = ERRw_EXT_SEM';
    ERRw_MOD_SEM = ERRw_MOD_SEM';
    ERRw_MIN_SEM = ERRw_MIN_SEM';
    
    ERRw_Table = table(Scan_session,ERRw_EXT_MEAN,ERRw_MOD_MEAN,ERRw_MIN_MEAN,ERRw_EXT_SEM,ERRw_MOD_SEM,ERRw_MIN_SEM);
    
    %% prepare a table to save the raw MT, RT, ERR and ERRw (useful for analysis conjoint to HOME-based sessions)
    
    % for every variable
    nSession = size(MT,4);
    
    % MT    
    temp = NaN*ones(size(MT,1)*size(MT,2),conf.Nseq,4);
    MT = reshape(MT,[size(MT,1)*size(MT,2),conf.Nseq,nSession]);
    temp(:,:,1:nSession) = MT;
    
    MT_Table_raw = table(temp(:,1,1),temp(:,2,1),temp(:,3,1),temp(:,4,1),temp(:,5,1),temp(:,6,1),...
        temp(:,1,2),temp(:,2,2),temp(:,3,2),temp(:,4,2),temp(:,5,2),temp(:,6,2),...
        temp(:,1,3),temp(:,2,3),temp(:,3,3),temp(:,4,3),temp(:,5,3),temp(:,6,3),...
        temp(:,1,4),temp(:,2,4),temp(:,3,4),temp(:,4,4),temp(:,5,4),temp(:,6,4),...
        'VariableNames',{'EXT1_s1','EXT2_s1','MOD1_s1','MOD2_s1','MIN1_s1','MIN2_s1',...
        'EXT1_s2','EXT2_s2','MOD1_s2','MOD2_s2','MIN1_s2','MIN2_s2',...
        'EXT1_s3','EXT2_s3','MOD1_s3','MOD2_s3','MIN1_s3','MIN2_s3',...
        'EXT1_s4','EXT2_s4','MOD1_s4','MOD2_s4','MIN1_s4','MIN2_s4'});
    
    % RT
    temp = NaN*ones(size(RT,1)*size(RT,2),conf.Nseq,4);
    RT = reshape(RT,[size(RT,1)*size(RT,2),conf.Nseq,nSession]);
    temp(:,:,1:nSession) = RT;
    
    RT_Table_raw = table(temp(:,1,1),temp(:,2,1),temp(:,3,1),temp(:,4,1),temp(:,5,1),temp(:,6,1),...
        temp(:,1,2),temp(:,2,2),temp(:,3,2),temp(:,4,2),temp(:,5,2),temp(:,6,2),...
        temp(:,1,3),temp(:,2,3),temp(:,3,3),temp(:,4,3),temp(:,5,3),temp(:,6,3),...
        temp(:,1,4),temp(:,2,4),temp(:,3,4),temp(:,4,4),temp(:,5,4),temp(:,6,4),...
        'VariableNames',{'EXT1_s1','EXT2_s1','MOD1_s1','MOD2_s1','MIN1_s1','MIN2_s1',...
        'EXT1_s2','EXT2_s2','MOD1_s2','MOD2_s2','MIN1_s2','MIN2_s2',...
        'EXT1_s3','EXT2_s3','MOD1_s3','MOD2_s3','MIN1_s3','MIN2_s3',...
        'EXT1_s4','EXT2_s4','MOD1_s4','MOD2_s4','MIN1_s4','MIN2_s4'});
    
    
    % ERR
    temp = NaN*ones(size(ERR,1)*size(ERR,2),conf.Nseq,4);
    ERR = reshape(ERR,[size(ERR,1)*size(ERR,2),conf.Nseq,nSession]);
    temp(:,:,1:nSession) = ERR;
    
    ERR_Table_raw = table(temp(:,1,1),temp(:,2,1),temp(:,3,1),temp(:,4,1),temp(:,5,1),temp(:,6,1),...
        temp(:,1,2),temp(:,2,2),temp(:,3,2),temp(:,4,2),temp(:,5,2),temp(:,6,2),...
        temp(:,1,3),temp(:,2,3),temp(:,3,3),temp(:,4,3),temp(:,5,3),temp(:,6,3),...
        temp(:,1,4),temp(:,2,4),temp(:,3,4),temp(:,4,4),temp(:,5,4),temp(:,6,4),...
        'VariableNames',{'EXT1_s1','EXT2_s1','MOD1_s1','MOD2_s1','MIN1_s1','MIN2_s1',...
        'EXT1_s2','EXT2_s2','MOD1_s2','MOD2_s2','MIN1_s2','MIN2_s2',...
        'EXT1_s3','EXT2_s3','MOD1_s3','MOD2_s3','MIN1_s3','MIN2_s3',...
        'EXT1_s4','EXT2_s4','MOD1_s4','MOD2_s4','MIN1_s4','MIN2_s4'});
    
    % ERRw
    temp = NaN*ones(size(ERRw,1)*size(ERRw,2),conf.Nseq,4);
    ERRw = reshape(ERRw,[size(ERRw,1)*size(ERRw,2),conf.Nseq,nSession]);
    temp(:,:,1:nSession) = ERRw;
    
    ERRw_Table_raw = table(temp(:,1,1),temp(:,2,1),temp(:,3,1),temp(:,4,1),temp(:,5,1),temp(:,6,1),...
        temp(:,1,2),temp(:,2,2),temp(:,3,2),temp(:,4,2),temp(:,5,2),temp(:,6,2),...
        temp(:,1,3),temp(:,2,3),temp(:,3,3),temp(:,4,3),temp(:,5,3),temp(:,6,3),...
        temp(:,1,4),temp(:,2,4),temp(:,3,4),temp(:,4,4),temp(:,5,4),temp(:,6,4),...
        'VariableNames',{'EXT1_s1','EXT2_s1','MOD1_s1','MOD2_s1','MIN1_s1','MIN2_s1',...
        'EXT1_s2','EXT2_s2','MOD1_s2','MOD2_s2','MIN1_s2','MIN2_s2',...
        'EXT1_s3','EXT2_s3','MOD1_s3','MOD2_s3','MIN1_s3','MIN2_s3',...
        'EXT1_s4','EXT2_s4','MOD1_s4','MOD2_s4','MIN1_s4','MIN2_s4'});
    
    
    %% save dataset
    save(outputDataset,'Data', ...
        'MT_Table', 'RT_Table','ERR_Table','ERRw_Table',...
        'MT_Table_raw','RT_Table_raw','ERR_Table_raw','ERRw_Table_raw',...
        'MTcorr','ORDERcorr','RTcorr')
    
    % clear variables
    clear MT RT ERR ERRw ERRt
    clear MTcorr ORDERcorr RTcorr
    clear MT_Table RT_Table ERR_Table ERRw_Table
    clear MT_Table_raw RT_Table_raw ERR_Table_raw ERRw_Table_raw
    clear temp
    
    
end  % end loop over subjects











