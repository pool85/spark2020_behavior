% DataElaboration_HOME_LAB.m
%
% SPARK PROJECT 2020.
%
% This is the last script to run amongs the other "elaboration scripts"
%
% This script brings together the data of each participant previously
% elaborated for the home- and lab-based sessions, and creates output MT,
% RT, ERR, ERRw and MT & RT of correct trials putting together data from
% laboratory and home-based sesssions in ordered sequence. 
%
% MT_HL, RT_HL, ERR_HL and ERRw_HL are ordered sequences from the first till
% the last lab-based session (including home-sessions) . These outputs are
% populated of NaN when laboratory- and/or home-based sessions are missing.
% 
% MTc_HL and RTc_HL are ordered sequences from lab and home sessions for
% CORRECT sequences only. These vector have to be used along with ORDc_HL,
% which contains the index of the correct sequences. 
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)
clear all
close all
clc

%% Initialize the script
addpath('..');
initAnalysis


%% Find file names and subject IDs (from HOME folder)
[csvFileNames, subjects] = getSubjectIds(conf.RAW_FOLDER_HOME);

nSubjects = length(subjects); % number of subjects


%% Analysis 
for i = 1:nSubjects
    
    SubjectsId = subjects{i};
    
    Sessions_HOME = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions_HOME = length(Sessions_HOME);
        
    inputDatasetLAB  = [ conf.EXTRACTED_FOLDER_LAB  filesep SubjectsId filesep SubjectsId '_elaborated.mat' ];
    inputDatasetHOME = [ conf.EXTRACTED_FOLDER_HOME filesep SubjectsId filesep SubjectsId '_elaborated.mat' ];
    outputDatasetLAB_HOME = [ conf.EXTRACTED_FOLDER_LAB_HOME filesep SubjectsId filesep SubjectsId '_elaborated1.mat' ];
    
    % Create folder if does not exist
    createFolderPath(outputDatasetLAB_HOME);
    
    
    %% initialize vectors that will contain behavioural outputs from home- and laboratory-based sessions together
    % MT
    MT_HL  = cell(conf.Nseq,1);
    MT_HL{1} = NaN * ones(conf.LAB_EXT_trials*conf.nsessLAB + conf.HOME_EXT_trials*conf.nsessHOME,1); MT_HL{2} = MT_HL{1};
    MT_HL{3} = NaN * ones(conf.LAB_MOD_trials*conf.nsessLAB + conf.HOME_MOD_trials*conf.nsessHOME,1); MT_HL{4} = MT_HL{3};
    MT_HL{5} = NaN * ones(conf.LAB_MIN_trials*conf.nsessLAB + conf.HOME_MIN_trials*conf.nsessHOME,1); MT_HL{6} = MT_HL{5};
    
    % RT, ERR and ERRw
    RT_HL   = MT_HL;
    ERR_HL  = MT_HL;
    ERRw_HL = MT_HL;
    
    %% load LAB data and store values for analysis (for correct trials only)
    load(inputDatasetLAB,'MT_Table_raw','RT_Table_raw','ERR_Table_raw','ERRw_Table_raw','MTcorr','RTcorr','ORDERcorr');
    
    % Only for correct trials: assign output
    MT_temp = MTcorr;
    RT_temp = RTcorr;
    ORD_temp = ORDERcorr;
    
    % Only for correct trials: elaborate vectors to be used
    nSessions_LAB = size(MT_temp,3); % number of completed LAB sessions for this participant 
    
    MTc_LAB = cell(conf.Nseq,nSessions_LAB);
    RTc_LAB = cell(conf.Nseq,nSessions_LAB);
    ORDc_LAB = cell(conf.Nseq,nSessions_LAB);
    
    for Nsess_LAB = 1:nSessions_LAB
        for Nseq = 1:conf.Nseq
            
            str_LAB  = conf.(['LAB_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);   % trials for the given sequence: LAB scans
            str_HOME = conf.(['HOME_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);  % trials for the given sequence: HOME training
            
            for Nepochs = 1:conf.LAB_Epochs
                
                MTc_LAB{Nseq,Nsess_LAB}  = [MTc_LAB{Nseq,Nsess_LAB};MT_temp{Nepochs,Nseq,Nsess_LAB}];
                RTc_LAB{Nseq,Nsess_LAB}  = [RTc_LAB{Nseq,Nsess_LAB};RT_temp{Nepochs,Nseq,Nsess_LAB}];
                ORDc_LAB{Nseq,Nsess_LAB} = [ORDc_LAB{Nseq,Nsess_LAB};ORD_temp{Nepochs,Nseq,Nsess_LAB} + (Nepochs-1)*conf.LAB_NSEQ_EPOCH + (Nsess_LAB-1)*str_LAB + (Nsess_LAB-1)*conf.nsessHOMEinterLAB*str_HOME];
                
            end
        end
    end
        
    clear MTcorr RTcorr ORDERcorr  MT_temp  RT_temp ORD_temp
    
    %% load HOME data and store values for analysis
    load(inputDatasetHOME,'MT','RT','ERR','ERRw','MTcorr','RTcorr','ORDERcorr');
    
    % Only for correct trials: assign output
    MTc_HOME = MTcorr;
    RTc_HOME = RTcorr;
    ORD_temp = ORDERcorr;
    
    % Only for correct trials: elaborate vectors to be used
    ORDc_HOME = cell(conf.Nseq,1);
    
    for Nseq = 1:conf.Nseq
        
        str_LAB  = conf.(['LAB_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);   % trials for the given sequence: LAB scans
        str_HOME = conf.(['HOME_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);  % trials for the given sequence: HOME training
        
        ORDc_HOME{Nseq} = ORD_temp{Nseq} + str_LAB; % baseline is now 51 and not anymore 1
        ORDc_HOME{Nseq}(ORD_temp{Nseq} > (str_HOME * conf.nsessHOMEinterLAB * 1)) = ORDc_HOME{Nseq}(ORD_temp{Nseq} > (str_HOME * conf.nsessHOMEinterLAB * 1)) + str_LAB ; 
        ORDc_HOME{Nseq}(ORD_temp{Nseq} > (str_HOME * conf.nsessHOMEinterLAB * 2)) = ORDc_HOME{Nseq}(ORD_temp{Nseq} > (str_HOME * conf.nsessHOMEinterLAB * 2)) + str_LAB ;
   
    end
    
    clear MTcorr RTcorr ORDERcorr ORD_temp
    
    
    %% Store, in the cell vectors initialized above, all the info from HOME and LAB sessions
    
    
    % Below, the work is done for all the trials, including incorrect ones
    
    for Nseq = 1:conf.Nseq
        
        str_LAB  = conf.(['LAB_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);   % trials for the given sequence: LAB scans
        str_HOME = conf.(['HOME_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);  % trials for the given sequence: HOME training
        
        
        % LAB
        for nLABsess = 1:conf.nsessLAB
            
            ind_i = (nLABsess-1)*(str_LAB+str_HOME*conf.nsessHOMEinterLAB) + 1;
            ind_f = (nLABsess-1)*(str_LAB+str_HOME*conf.nsessHOMEinterLAB) + str_LAB;
            
            MT_HL{Nseq}(ind_i:ind_f,1)    = MT_Table_raw.([conf.(['seq',num2str(Nseq)]),'_s',num2str(nLABsess)]);
            RT_HL{Nseq}(ind_i:ind_f,1)    = RT_Table_raw.([conf.(['seq',num2str(Nseq)]),'_s',num2str(nLABsess)]);
            ERR_HL{Nseq}(ind_i:ind_f,1)   = ERR_Table_raw.([conf.(['seq',num2str(Nseq)]),'_s',num2str(nLABsess)]);
            ERRw_HL{Nseq}(ind_i:ind_f,1)  = ERRw_Table_raw.([conf.(['seq',num2str(Nseq)]),'_s',num2str(nLABsess)]);
            
        end
        
        
        % HOME
        for nHOMEsess = 1:nSessions_HOME
            
            if nHOMEsess > 20 % more than 20-home based sessions are done
                ind_start = (conf.nsessLAB - 1)*str_LAB + 2*(str_HOME * conf.nsessHOMEinterLAB);
                ind_i = ind_start + (nHOMEsess-20-1)*str_HOME + 1;
                ind_f = ind_start + (nHOMEsess-20)*str_HOME;
                
            elseif nHOMEsess > 10  % more than 10-home based sessions are done
                ind_start = (conf.nsessLAB - 2)*str_LAB + (str_HOME * conf.nsessHOMEinterLAB);
                ind_i = ind_start + (nHOMEsess-10-1)*str_HOME + 1;
                ind_f = ind_start + (nHOMEsess-10)*str_HOME;
                
            else  % less than 10-home based sessions are done
                ind_start = (conf.nsessLAB - 3)*str_LAB;
                ind_i = ind_start + (nHOMEsess-1)*str_HOME + 1;
                ind_f = ind_start + (nHOMEsess)*str_HOME;
                
            end
            
            MT_HL{Nseq}(ind_i:ind_f,1)   = MT{Nseq}((nHOMEsess-1)*str_HOME + 1:(nHOMEsess*str_HOME));
            RT_HL{Nseq}(ind_i:ind_f,1)   = RT{Nseq}((nHOMEsess-1)*str_HOME + 1:(nHOMEsess*str_HOME));
            ERR_HL{Nseq}(ind_i:ind_f,1)  = ERR{Nseq}((nHOMEsess-1)*str_HOME + 1:(nHOMEsess*str_HOME));
            ERRw_HL{Nseq}(ind_i:ind_f,1) = ERRw{Nseq}((nHOMEsess-1)*str_HOME + 1:(nHOMEsess*str_HOME));
            
        end
           
    end  % end of sequence loop
    
    
    
    % Below, the work is done for correct trails. We put together only
    %        correct trials from lab and home sessions.
    
    for Nseq = 1:conf.Nseq
        
        temp_ord = [];
        temp_mt = [];
        temp_rt = [];
        
        for NsessLAB = 1:nSessions_LAB
            temp_ord = [temp_ord;ORDc_LAB{Nseq,NsessLAB}];
            temp_mt  = [temp_mt;MTc_LAB{Nseq,NsessLAB}];
            temp_rt  = [temp_rt;RTc_LAB{Nseq,NsessLAB}];
        end
        temp_ord = [temp_ord;ORDc_HOME{Nseq}];
        temp_mt  = [temp_mt;MTc_HOME{Nseq}];
        temp_rt  = [temp_rt;RTc_HOME{Nseq}];

        
        [ORDc_HL{Nseq},sort_ind] = sort(temp_ord,'ascend');
        
        MTc_HL{Nseq} = temp_mt(sort_ind);
        
        RTc_HL{Nseq} = temp_rt(sort_ind);
    
    end  % end of sequence loop
    
    
    %% Save outputs for the analysis scripts
    save(outputDatasetLAB_HOME,'MT_HL','RT_HL','ERR_HL','ERRw_HL','MTc_HL','RTc_HL','ORDc_HL')
    
    
    %% clear variables
    clear temp_ord temp_mt temp_rt MT_HL RT_HL ERR_HL ERRw_HL MTc_HL RTc_HL ORDc_HL
    
end




