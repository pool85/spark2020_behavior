% DataAnalysis_FIT.m
%
% SPARK PROJECT 2020.
%
% This script loads the data eleborated from the HOME and LAB-based
% training sesions, and performs the FIT of the MT. Fitting is done on 4
% different outcomes:
% 1) correct MT from HOME sessions
% 2) correct MT from LAB + HOME sessions
% 3) MT (all trials) from HOME sessions
% 4) MT (all trials) from LAB + HOME sessions
%
% 
%
% TODO: 1) value of the fitted model at fixed trials, for visualization
%       purposes (only if necessary) 
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)
clear all
close all
clc

%% Initialize the script
addpath('..');
initAnalysis


%% OPTIONS
flagfigsave = 0;
flagfig = 1;

%% Find file names and subject IDs
[~ , subjects] = getSubjectIds(conf.RAW_FOLDER_HOME);


%% Eclude subjects from analysis
vec_ex = [1 2 3  5 6 7 8 9 10 11 12 13 14 15 16 17 18];
%vec_ex = [1 2 4 5 7 10];
%vec_ex = [];

ID_exclude ={};
for i=vec_ex
    
ID_exclude = [ID_exclude,subjects{i}]; % ID of participants to exclude for the moment (e.g., participants that just started the protocol)

end

if ~isempty(ID_exclude)
    for i = 1:length(ID_exclude)
        subjects(contains(subjects,ID_exclude{i})) = [];
    end
end

nSubjects = length(subjects); % number of subjects


%% Loop over the subjects to see which is the maximum common number of HOME-based sessions performed by each participant
[csvFileNames, ~] = getSubjectIds(conf.RAW_FOLDER_HOME); 

for i = 1:nSubjects
    
    SubjectsId = subjects{i};
    
    Sessions_HOME = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions_HOME = length(Sessions_HOME);
    
    SbjSessions_HOME(i) = nSessions_HOME;
    
end  % end loop over subjects

MaxSessionsHOME = min(SbjSessions_HOME);

disp(['The maximum common number of HOME-based sessions performed by each participant is ',num2str(MaxSessionsHOME)])


%% Loop over the subjects to see which is the maximum common number of LAB-based sessions performed by each participant
[csvFileNames, ~] = getSubjectIds(conf.RAW_FOLDER_LAB); 

for i = 1:nSubjects
    
    SubjectsId = subjects{i};
    
    Sessions_LAB = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions_LAB = length(Sessions_LAB);
    
    SbjSessions_LAB(i) = nSessions_LAB;
    
end  % end loop over subjects

MaxSessionsLAB = min(SbjSessions_LAB);

disp(['The maximum common number of LAB-based sessions performed by each participant is ',num2str(MaxSessionsLAB)])


%% Analysis: prepare dataset 

for i = 1:nSubjects
     
     SubjectsId = subjects{i};
     
     inputDatasetLAB_HOME  = [ conf.EXTRACTED_FOLDER_LAB_HOME  filesep SubjectsId filesep SubjectsId '_elaborated1.mat' ];
     
     
     %% load elaborated data with home- and lab-based together
     load(inputDatasetLAB_HOME,'MT_HL','RT_HL','ERR_HL','MTc_HL','RTc_HL','ORDc_HL');

     
     %% Home-based sessions only: all trials
     for Nseq = 1:conf.Nseq
         
        str_LAB  = conf.(['LAB_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);   % trials for the given sequence: LAB scans
        str_HOME = conf.(['HOME_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);  % trials for the given sequence: HOME training
        
        % eliminate the data of the 4 lab scans
        temp_MT  = MT_HL{Nseq};
        temp_RT  = RT_HL{Nseq};
        temp_ERR = ERR_HL{Nseq};
        
        ind_i_sc1 = 1;
        ind_f_sc1 = (ind_i_sc1-1) + str_LAB;
        
        ind_i_sc2 = ind_f_sc1 + str_HOME*conf.nsessHOMEinterLAB + 1;
        ind_f_sc2 = (ind_i_sc2-1) + str_LAB;
        
        ind_i_sc3 = ind_f_sc2 + str_HOME*conf.nsessHOMEinterLAB + 1;
        ind_f_sc3 = (ind_i_sc3-1) + str_LAB;
        
        ind_i_sc4 = ind_f_sc3 + str_HOME*conf.nsessHOMEinterLAB + 1;
        ind_f_sc4 = (ind_i_sc4-1) + str_LAB;
        
        temp_MT([(ind_i_sc1:ind_f_sc1)';(ind_i_sc2:ind_f_sc2)';(ind_i_sc3:ind_f_sc3)';(ind_i_sc4:ind_f_sc4)']) = [];
        temp_RT([(ind_i_sc1:ind_f_sc1)';(ind_i_sc2:ind_f_sc2)';(ind_i_sc3:ind_f_sc3)';(ind_i_sc4:ind_f_sc4)']) = [];
        temp_ERR([(ind_i_sc1:ind_f_sc1)';(ind_i_sc2:ind_f_sc2)';(ind_i_sc3:ind_f_sc3)';(ind_i_sc4:ind_f_sc4)']) = [];
        
        % MT: take non-NaN elements from MT 
        MT_H_sbj{Nseq,i} = temp_MT(~isnan(temp_MT)); % create vector for all subjects containing the trials for the home-based sessions
        
        % RT: take non-NaN elements from RT 
        RT_H_sbj{Nseq,i} = temp_RT(~isnan(temp_RT)); % create vector for all subjects containing the trials for the home-based sessions
        
        % ERR: take non-NaN elements from ERR
        ERR_H_sbj{Nseq,i} = temp_ERR(~isnan(temp_ERR)); % create vector for all subjects containing the trials for the home-based sessions
         
     end
     
     
     %% Lab- and Home-based sessions: all trials
     for Nseq = 1:conf.Nseq
         
        str_LAB  = conf.(['LAB_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);   % trials for the given sequence: LAB scans
        str_HOME = conf.(['HOME_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);  % trials for the given sequence: HOME training
        
        % assign temp variables
        temp_MT  = MT_HL{Nseq};
        temp_RT  = RT_HL{Nseq};
        temp_ERR = ERR_HL{Nseq};
                
        % MT: take non-NaN elements from MT 
        MT_HL_sbj{Nseq,i} = temp_MT(~isnan(temp_MT)); % create vector for all subjects containing the trials for the home-based sessions
        
        % RT: take non-NaN elements from RT 
        RT_HL_sbj{Nseq,i} = temp_RT(~isnan(temp_RT)); % create vector for all subjects containing the trials for the home-based sessions
        
        % ERR: take non-NaN elements from ERR
        ERR_HL_sbj{Nseq,i} = temp_ERR(~isnan(temp_ERR)); % create vector for all subjects containing the trials for the home-based sessions
         
     end
     
     
     %% Home-based sessions only: correct trials
     for Nseq = 1:conf.Nseq
         
        str_LAB  = conf.(['LAB_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);   % trials for the given sequence: LAB scans
        str_HOME = conf.(['HOME_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);  % trials for the given sequence: HOME training
        
         % eliminate the data of the 4 lab scans
         temp_MTc   = MTc_HL{Nseq};
         temp_RTc   = RTc_HL{Nseq};
         temp_ORDc  = ORDc_HL{Nseq};
       
         ind_i_sc1 = 1;
         ind_f_sc1 = (ind_i_sc1-1) + str_LAB;
         
         ind_i_sc2 = ind_f_sc1 + str_HOME*conf.nsessHOMEinterLAB + 1;
         ind_f_sc2 = (ind_i_sc2-1) + str_LAB;
         
         ind_i_sc3 = ind_f_sc2 + str_HOME*conf.nsessHOMEinterLAB + 1;
         ind_f_sc3 = (ind_i_sc3-1) + str_LAB;
         
         ind_i_sc4 = ind_f_sc3 + str_HOME*conf.nsessHOMEinterLAB + 1;
         ind_f_sc4 = (ind_i_sc4-1) + str_LAB;
         
         ind_include = or(or(and(temp_ORDc > ind_f_sc1, temp_ORDc < ind_i_sc2),and(temp_ORDc > ind_f_sc2, temp_ORDc < ind_i_sc3)),and(temp_ORDc > ind_f_sc3, temp_ORDc < ind_i_sc4));
         
         temp_MTc = temp_MTc(ind_include);
         temp_RTc = temp_RTc(ind_include);
         temp_ORDc = temp_ORDc(ind_include);
         
         % adjust indexes to account for the missing home-based sessions
         temp_ORDc(temp_ORDc>ind_f_sc3) = temp_ORDc(temp_ORDc>ind_f_sc3) - str_LAB; % adjust w.r.t to third lab session
         temp_ORDc(temp_ORDc>ind_f_sc2) = temp_ORDc(temp_ORDc>ind_f_sc2) - str_LAB; % adjust w.r.t to second lab session
         temp_ORDc(temp_ORDc>ind_f_sc1) = temp_ORDc(temp_ORDc>ind_f_sc1) - str_LAB; % adjust w.r.t to first lab session
         
         % MTc:  
         MTc_H_sbj{Nseq,i} = temp_MTc; % create vector for all subjects containing the correct trials for the home-based sessions
         
         % RTc: 
         RTc_H_sbj{Nseq,i} = temp_RTc; % create vector for all subjects containing the correct trials for the home-based sessions
         
         % ORDc:
         ORDc_H_sbj{Nseq,i} = temp_ORDc; % create vector for all subjects containing the correct trials for the home-based sessions
         
     end
     
     
     %% Lab- and Home-based sessions: correct trials
     for Nseq = 1:conf.Nseq
         
         str_LAB  = conf.(['LAB_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);   % trials for the given sequence: LAB scans
         str_HOME = conf.(['HOME_',conf.(['seq',num2str(Nseq)])(1:3),'_trials']);  % trials for the given sequence: HOME training
         
          % assign temporary variables
          temp_MTc   = MTc_HL{Nseq};
          temp_RTc   = RTc_HL{Nseq};
          temp_ORDc  = ORDc_HL{Nseq};
        
          ind_i_sc1 = 1;
          ind_f_sc1 = (ind_i_sc1-1) + str_LAB;
          
          ind_i_sc2 = ind_f_sc1 + str_HOME*conf.nsessHOMEinterLAB + 1;
          ind_f_sc2 = (ind_i_sc2-1) + str_LAB;
          
          ind_i_sc3 = ind_f_sc2 + str_HOME*conf.nsessHOMEinterLAB + 1;
          ind_f_sc3 = (ind_i_sc3-1) + str_LAB;
          
          ind_i_sc4 = ind_f_sc3 + str_HOME*conf.nsessHOMEinterLAB + 1;
          ind_f_sc4 = (ind_i_sc4-1) + str_LAB;
                    
          % adjust indexes to account for the missing home-based sessions
          if SbjSessions_LAB(i) == 1
              
              temp_ORDc(temp_ORDc>ind_f_sc3) = temp_ORDc(temp_ORDc>ind_f_sc3) - str_LAB; % adjust w.r.t to third lab session
              temp_ORDc(temp_ORDc>ind_f_sc2) = temp_ORDc(temp_ORDc>ind_f_sc2) - str_LAB; % adjust w.r.t to second lab session
              
          elseif SbjSessions_LAB(i) == 2
              
              temp_ORDc(temp_ORDc>ind_f_sc3) = temp_ORDc(temp_ORDc>ind_f_sc3) - str_LAB; % adjust w.r.t to third lab session
              
          end
          
          % MTc:  
          MTc_HL_sbj{Nseq,i} = temp_MTc; % create vector for all subjects containing the correct trials for the home-based sessions
          
          % RTc: 
          RTc_HL_sbj{Nseq,i} = temp_RTc; % create vector for all subjects containing the correct trials for the home-based sessions
          
          % ORDc:
          ORDc_HL_sbj{Nseq,i} = temp_ORDc; % create vector for all subjects containing the correct trials for the home-based sessions
         
     end
     
     
end


%% -------------------------------------------------------------------------%
% FIT. Type: a*exp(x*b) + c; HOME sessions, CORRECT TRIALS only 

% options for plots
axis_vector = [0 2120 0 7];

for i = 1:nSubjects
    
    for Nseq = 1:2 % compute for EXT only
        
        seq_str = conf.(['seq',num2str(Nseq)]);
        
        X = ORDc_H_sbj{Nseq,i};
        Y = MTc_H_sbj{Nseq,i};
        
        
        [FIT_HOME_CORRECT_d0.(subjects{i}).(seq_str), GOF_HOME_CORRECT_d0.(subjects{i}).(seq_str)] = Fit_2exp_d0(X, Y, axis_vector,['HOME sessions (CORRECT); ',subjects{i},' ',seq_str],flagfig);

        % save figure
        if flagfigsave 
            saveas(figure(1),[conf.PLOT_FOLDER_FIT filesep 'HOMEcorrect_d0_',subjects{i},'_',seq_str,'.png'])
            close all
        end
        
        
        [FIT_HOME_CORRECT_rob.(subjects{i}).(seq_str), GOF_HOME_CORRECT_rob.(subjects{i}).(seq_str)] = Fit_2exp_Robust(X, Y, axis_vector,['HOME sessions (CORRECT); ',subjects{i},' ',seq_str],flagfig);
        
        % save figure
        if flagfigsave 
            saveas(figure(1),[conf.PLOT_FOLDER_FIT filesep 'HOMEcorrect_rob_',subjects{i},'_',seq_str,'.png'])
            close all
        end
        
        
        [FIT_HOME_CORRECT_Nrob.(subjects{i}).(seq_str), GOF_HOME_CORRECT_Nrob.(subjects{i}).(seq_str)] = Fit_2exp_NoRobust(X, Y, axis_vector,['HOME sessions (CORRECT); ',subjects{i},' ',seq_str],flagfig);
        
        % save figure
        if flagfigsave 
            saveas(figure(1),[conf.PLOT_FOLDER_FIT filesep 'HOMEcorrect_Nrob_',subjects{i},'_',seq_str,'.png'])
            close all
        end
        
        
    end
    
end


%% -------------------------------------------------------------------------%
% FIT. Type: a*exp(x*b) + c; HOME sessions, ALL TRIALS 
 
% options for plots
axis_vector = [0 2120 0 7];

for i = 1:nSubjects
    
    for Nseq = 1:2 % compute for EXT only
        
        seq_str = conf.(['seq',num2str(Nseq)]);
        
        Y = MT_H_sbj{Nseq,i};
        X = (1:length(Y))';
        
        [FIT_HOME_d0.(subjects{i}).(seq_str), GOF_HOME_d0.(subjects{i}).(seq_str)] = Fit_2exp_d0(X, Y, axis_vector,['HOME sessions (All trials); ',subjects{i},' ',seq_str],flagfig);
        
         % save figure
        if flagfigsave 
            saveas(figure(1),[conf.PLOT_FOLDER_FIT filesep 'HOMEalltrials_d0_',subjects{i},'_',seq_str,'.png'])
            close all
        end
        
        
        [FIT_HOME_rob.(subjects{i}).(seq_str), GOF_HOME_rob.(subjects{i}).(seq_str)] = Fit_2exp_Robust(X, Y, axis_vector,['HOME sessions (All trials); ',subjects{i},' ',seq_str],flagfig);
        
         % save figure
        if flagfigsave 
            saveas(figure(1),[conf.PLOT_FOLDER_FIT filesep 'HOMEalltrials_rob_',subjects{i},'_',seq_str,'.png'])
            close all
        end
        
        
        [FIT_HOME_Nrob.(subjects{i}).(seq_str), GOF_HOME_Nrob.(subjects{i}).(seq_str)] = Fit_2exp_NoRobust(X, Y, axis_vector,['HOME sessions (All trials); ',subjects{i},' ',seq_str],flagfig);
        
         % save figure
        if flagfigsave 
            saveas(figure(1),[conf.PLOT_FOLDER_FIT filesep 'HOMEalltrials_Nrob_',subjects{i},'_',seq_str,'.png'])
            close all
        end
        
    end
    
end


%% write data to a table and save it

% prepare variables for the table: HOME CORRECT

for i = 1:nSubjects
    
    % first equation
    doub_exp_d0_EXT1(i,1) = FIT_HOME_CORRECT_d0.(subjects{i}).EXT1.a;
    doub_exp_d0_EXT1(i,2) = FIT_HOME_CORRECT_d0.(subjects{i}).EXT1.b;
    doub_exp_d0_EXT1(i,3) = FIT_HOME_CORRECT_d0.(subjects{i}).EXT1.c;
    doub_exp_d0_EXT1(i,4) = 0;
    
    doub_exp_d0_EXT2(i,1) = FIT_HOME_CORRECT_d0.(subjects{i}).EXT2.a;
    doub_exp_d0_EXT2(i,2) = FIT_HOME_CORRECT_d0.(subjects{i}).EXT2.b;
    doub_exp_d0_EXT2(i,3) = FIT_HOME_CORRECT_d0.(subjects{i}).EXT2.c;
    doub_exp_d0_EXT2(i,4) = 0;
    
    
    % second equation
    doub_exp_rob_EXT1(i,1) = FIT_HOME_CORRECT_rob.(subjects{i}).EXT1.a;
    doub_exp_rob_EXT1(i,2) = FIT_HOME_CORRECT_rob.(subjects{i}).EXT1.b;
    doub_exp_rob_EXT1(i,3) = FIT_HOME_CORRECT_rob.(subjects{i}).EXT1.c;
    doub_exp_rob_EXT1(i,4) = FIT_HOME_CORRECT_rob.(subjects{i}).EXT1.d;
    
    doub_exp_rob_EXT2(i,1) = FIT_HOME_CORRECT_rob.(subjects{i}).EXT2.a;
    doub_exp_rob_EXT2(i,2) = FIT_HOME_CORRECT_rob.(subjects{i}).EXT2.b;
    doub_exp_rob_EXT2(i,3) = FIT_HOME_CORRECT_rob.(subjects{i}).EXT2.c;
    doub_exp_rob_EXT2(i,4) = FIT_HOME_CORRECT_rob.(subjects{i}).EXT2.d;
    
    
    % third equation
    doub_exp_Nrob_EXT1(i,1) = FIT_HOME_CORRECT_Nrob.(subjects{i}).EXT1.a;
    doub_exp_Nrob_EXT1(i,2) = FIT_HOME_CORRECT_Nrob.(subjects{i}).EXT1.b;
    doub_exp_Nrob_EXT1(i,3) = FIT_HOME_CORRECT_Nrob.(subjects{i}).EXT1.c;
    doub_exp_Nrob_EXT1(i,4) = FIT_HOME_CORRECT_Nrob.(subjects{i}).EXT1.d;
    
    doub_exp_Nrob_EXT2(i,1) = FIT_HOME_CORRECT_Nrob.(subjects{i}).EXT2.a;
    doub_exp_Nrob_EXT2(i,2) = FIT_HOME_CORRECT_Nrob.(subjects{i}).EXT2.b;
    doub_exp_Nrob_EXT2(i,3) = FIT_HOME_CORRECT_Nrob.(subjects{i}).EXT2.c;
    doub_exp_Nrob_EXT2(i,4) = FIT_HOME_CORRECT_Nrob.(subjects{i}).EXT2.d;
end


TABLE_FIT_HOME_CORRECT = table(subjects',doub_exp_d0_EXT1,doub_exp_rob_EXT1,doub_exp_Nrob_EXT1,doub_exp_d0_EXT2,doub_exp_rob_EXT2,doub_exp_Nrob_EXT2);


% prepare variables for the table: HOME all TRIALS

for i = 1:nSubjects
    
    % first equation
    doub_exp_d0_EXT1(i,1) = FIT_HOME_d0.(subjects{i}).EXT1.a;
    doub_exp_d0_EXT1(i,2) = FIT_HOME_d0.(subjects{i}).EXT1.b;
    doub_exp_d0_EXT1(i,3) = FIT_HOME_d0.(subjects{i}).EXT1.c;
    doub_exp_d0_EXT1(i,4) = 0;
    
    doub_exp_d0_EXT2(i,1) = FIT_HOME_d0.(subjects{i}).EXT2.a;
    doub_exp_d0_EXT2(i,2) = FIT_HOME_d0.(subjects{i}).EXT2.b;
    doub_exp_d0_EXT2(i,3) = FIT_HOME_d0.(subjects{i}).EXT2.c;
    doub_exp_d0_EXT2(i,4) = 0;
    
    
    % second equation
    doub_exp_rob_EXT1(i,1) = FIT_HOME_rob.(subjects{i}).EXT1.a;
    doub_exp_rob_EXT1(i,2) = FIT_HOME_rob.(subjects{i}).EXT1.b;
    doub_exp_rob_EXT1(i,3) = FIT_HOME_rob.(subjects{i}).EXT1.c;
    doub_exp_rob_EXT1(i,4) = FIT_HOME_rob.(subjects{i}).EXT1.d;
    
    doub_exp_rob_EXT2(i,1) = FIT_HOME_rob.(subjects{i}).EXT2.a;
    doub_exp_rob_EXT2(i,2) = FIT_HOME_rob.(subjects{i}).EXT2.b;
    doub_exp_rob_EXT2(i,3) = FIT_HOME_rob.(subjects{i}).EXT2.c;
    doub_exp_rob_EXT2(i,4) = FIT_HOME_rob.(subjects{i}).EXT2.d;
    
    
    % third equation
    doub_exp_Nrob_EXT1(i,1) = FIT_HOME_Nrob.(subjects{i}).EXT1.a;
    doub_exp_Nrob_EXT1(i,2) = FIT_HOME_Nrob.(subjects{i}).EXT1.b;
    doub_exp_Nrob_EXT1(i,3) = FIT_HOME_Nrob.(subjects{i}).EXT1.c;
    doub_exp_Nrob_EXT1(i,4) = FIT_HOME_Nrob.(subjects{i}).EXT1.d;
    
    doub_exp_Nrob_EXT2(i,1) = FIT_HOME_Nrob.(subjects{i}).EXT2.a;
    doub_exp_Nrob_EXT2(i,2) = FIT_HOME_Nrob.(subjects{i}).EXT2.b;
    doub_exp_Nrob_EXT2(i,3) = FIT_HOME_Nrob.(subjects{i}).EXT2.c;
    doub_exp_Nrob_EXT2(i,4) = FIT_HOME_Nrob.(subjects{i}).EXT2.d;
end


TABLE_FIT_HOMEalltrials = table(subjects',doub_exp_d0_EXT1,doub_exp_rob_EXT1,doub_exp_Nrob_EXT1,doub_exp_d0_EXT2,doub_exp_rob_EXT2,doub_exp_Nrob_EXT2);


% save 
save([conf.RESULTS_FOLDER_FIT filesep 'FIT_TABLES.mat'],'TABLE_FIT_HOMEalltrials','TABLE_FIT_HOME_CORRECT')

















% %% -------------------------------------------------------------------------%
% % FIT. Type: a*exp(x*b) + c; HOME and LAB sessions, CORRECT TRIALS only 
% 
% % options for plots
% axis_vector = [0 2120 0 7];
% 
% for i = 1:nSubjects
%     
%     for Nseq = 1:2 % compute for EXT only
%         
%         seq_str = conf.(['seq',num2str(Nseq)]);
%         
%         X = ORDc_HL_sbj{Nseq,i};
%         Y = MTc_HL_sbj{Nseq,i};
%         [FIT_HOME_LAB_CORRECT.(subjects{i}).(seq_str), GOF_HOME_LAB_CORRECT.(subjects{i}).(seq_str)] = Fit_2exp_d0(X, Y, axis_vector,['HOME&LAB sessions (CORRECT); ',subjects{i},' ',seq_str],1);
% 
%         % save figure
%         if flagfigsave 
%             saveas(figure(1),[conf.PLOT_FOLDER_FIT filesep 'HOME&LABcorrect_',subjects{i},'_',seq_str,'.png'])
%             close all
%         end    
%     
%     end
%     
% end
% 
% 
% %% -------------------------------------------------------------------------%
% % FIT. Type: a*exp(x*b) + c; HOME and LAB sessions, ALL TRIALS
% 
% % options for plots
% axis_vector = [0 2120 0 7];
% 
% for i = 1:nSubjects
%     
%     for Nseq = 1:2 % compute for EXT only
%         
%         seq_str = conf.(['seq',num2str(Nseq)]);
%         
%         Y = MT_HL_sbj{Nseq,i};
%         X = (1:length(Y))';
%         
%         [FIT_HOME_LAB.(subjects{i}).(seq_str), GOF_HOME_LAB.(subjects{i}).(seq_str)] = Fit_2exp_d0(X, Y, axis_vector,['HOME&LAB sessions (Alltrials); ',subjects{i},' ',seq_str],1);
% 
%          % save figure
%         if flagfigsave 
%             saveas(figure(1),[conf.PLOT_FOLDER_FIT filesep 'HOME&LABalltrials_',subjects{i},'_',seq_str,'.png'])
%             close all
%         end        
%         
%     end
%     
% end

















% %% Determine a time in training when a subject's performance reached a plateau  (0.25 ms [ == 0.00025 sec]). See Wymbs et al., 2014. 
% % NB: THIS IS VALID FOR equation a*exp(bx) + c !!!! 
% 
% % EQUATION: X_plateau = ln(0.00025/(a*(1-exp(b))))/(b)
% 
% PL_TRS = 0.00025; % treshold for plateau
% 
% 
% % Look for plateau
% 
% for i = 1:nSubjects
%     
%     for Nseq = 1:2 % compute for EXT only
%         
%         seq_str = conf.(['seq',num2str(Nseq)]);
%         
%         % Home, correct only
%         aHc = FIT_HOME_CORRECT.(subjects{i}).(seq_str).a;
%         bHc = FIT_HOME_CORRECT.(subjects{i}).(seq_str).b;
%         
%         PlateauTrial.HOME_CORRECT(Nseq,i) =  log(PL_TRS/(aHc*(1-exp(bHc)))) / (bHc);
%         
%         % Home&LAB, correct only
%         aHLc = FIT_HOME_LAB_CORRECT.(subjects{i}).(seq_str).a;
%         bHLc = FIT_HOME_LAB_CORRECT.(subjects{i}).(seq_str).b;
%         
%         PlateauTrial.HOME_LAB_CORRECT(Nseq,i) =  log(PL_TRS/(aHLc*(1-exp(bHLc)))) / (bHLc);
%         
%         % Home, all trials  
%         aH = FIT_HOME.(subjects{i}).(seq_str).a;
%         bH = FIT_HOME.(subjects{i}).(seq_str).b;
%         
%         PlateauTrial.HOME(Nseq,i) =  log(PL_TRS/(aH*(1-exp(bH)))) / (bH);
%         
%         % Home&LAB, all trials
%         aHL = FIT_HOME_LAB.(subjects{i}).(seq_str).a;
%         bHL = FIT_HOME_LAB.(subjects{i}).(seq_str).b;
%         
%         PlateauTrial.HOME_LAB(Nseq,i) =  log(PL_TRS/(aHL*(1-exp(bHL)))) / (bHL);
%         
%     end
%     
% end
% 
% 
% %% Determine the value of the model at fixed trials: TODO
% % NB: THIS IS VALID FOR equation a*exp(bx) + c !!!! 
% 
% % for i = 1:nSubjects
% %     
% %     for Nseq = 1:2 % compute for EXT only
% %         
% %         seq_str = conf.(['seq',num2str(Nseq)]);
% %         
% %         aHc = FIT_HOME_CORRECT.(subjects{i}).(seq_str).a;
% %         bHc = FIT_HOME_CORRECT.(subjects{i}).(seq_str).b;
% %         cHc = FIT_HOME_CORRECT.(subjects{i}).(seq_str).c;
% %         
% %         TRIAL = 1;
% %         MTestimate.HOME_CORRECT_TRIAL1(Nseq,i) = aHc*exp(bHc*TRIAL) + cHc;
% %         TRIAL = 300;
% %         MTestimate.HOME_CORRECT_TRIAL2(Nseq,i) = aHc*exp(bHc*TRIAL) + cHc;
% %         TRIAL = 1000;
% %         MTestimate.HOME_CORRECT_TRIAL3(Nseq,i) = aHc*exp(bHc*TRIAL) + cHc;
% %         TRIAL = 1900;
% %         MTestimate.HOME_CORRECT_TRIAL4(Nseq,i) = aHc*exp(bHc*TRIAL) + cHc;
% %         
% %     end
% %     
% % end
% % Data = [MTestimate.HOME_CORRECT_TRIAL1(1,:)',MTestimate.HOME_CORRECT_TRIAL2(1,:)',MTestimate.HOME_CORRECT_TRIAL3(1,:)',MTestimate.HOME_CORRECT_TRIAL4(1,:)'];
% % [est,HDI]=rst_data_plot(Data,'between',0.25,'within',0.025,'pointsize',50,'estimator','median','kernel','on');
% 
% 
% 
% 
% 
% %saveas(figure(1),[conf.PLOT_FOLDER_PRAC_INT filesep 'GrandAverage_MovTime_binsize_',num2str(BIN_SIZE),'.png'])