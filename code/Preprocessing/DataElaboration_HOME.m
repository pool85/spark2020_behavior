% DataElaboration_HOME.m
%
% SPARK PROJECT 2020.
%
% Get Movement Time (MT), Error rates (ERR) and Response Time (RT) from laboratory sessions. The data are organized
% according to participants, sessions, and epochs, in tables
%
% ERR is == 1 if any error has been committed during sequence execution
% ERRw displays the proportion of errors committed in each sequence, and is defined between 0 and 1
%
% NB: the script also detect outliers in the MT and RT, and replace these
% values with the average from the trials before and after.
%
% Last mod: 09.11.2020 corrected ERRw calculation and added another ERR
% vector informing about the number of total mistakes in a sequence
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)
clc
clear all

%% Initialize the script
addpath('..');
initAnalysis

%% Options
Outlier_thrshold_MT = 20; % in seconds
Outlier_thrshold_RT = 3;  % in seconds

%% Find file names and subject IDs
[csvFileNames, subjects] = getSubjectIds(conf.RAW_FOLDER_HOME);

nSubjects = length(subjects); % number of subjects


%% For each subject, get data from each scan session

for i = 1:nSubjects
    
    SubjectsId = subjects{i};
    
    Sessions = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions = length(Sessions);
    
    SbjSessions(i) = nSessions;
    
    inputDataset  = [ conf.EXTRACTED_FOLDER_HOME filesep SubjectsId filesep SubjectsId '.mat'];
    outputDataset = [ conf.EXTRACTED_FOLDER_HOME filesep SubjectsId filesep SubjectsId '_elaborated.mat'];
    
    load(inputDataset,'Data');
    
    
    %% Exctract subject's data
    
    % initialize cell vectors
    MT = cell(conf.Nseq,1);
    MTcorr = cell(conf.Nseq,1);
    RT = cell(conf.Nseq,1);
    RTcorr = cell(conf.Nseq,1);
    ERR = cell(conf.Nseq,1);
    ERRw = cell(conf.Nseq,1);
    ORDERcorr = cell(conf.Nseq,1);
    
    
    for nsess = 1:nSessions
        
        session_str = sprintf('Session_%03d',nsess);
        
        % Get display number from dispRep vector
        Data.(session_str).dispNum = trialsRep2dspNum(Data.(session_str).dispRep,10)';
        
        % 1) MT: Movement time. seqDuration is the sum of the first responses to DSP displays (whther they are correct or not).
        %     If you want the global duration (i.e., the sum to all displays, select seqDurationAll). To get the real MT, if you
        %     will have to substract the RT to the first DSP display in each trial
        MT{1} = [MT{1};Data.(session_str).seqDuration(contains(Data.(session_str).seqType,conf.seq1))];
        MT{2} = [MT{2};Data.(session_str).seqDuration(contains(Data.(session_str).seqType,conf.seq2))];
        MT{3} = [MT{3};Data.(session_str).seqDuration(contains(Data.(session_str).seqType,conf.seq3))];
        MT{4} = [MT{4};Data.(session_str).seqDuration(contains(Data.(session_str).seqType,conf.seq4))];
        MT{5} = [MT{5};Data.(session_str).seqDuration(contains(Data.(session_str).seqType,conf.seq5))];
        MT{6} = [MT{6};Data.(session_str).seqDuration(contains(Data.(session_str).seqType,conf.seq6))];
        
        
        % 2) MT of correct trials only
        MTcorr{1} = [MTcorr{1};Data.(session_str).seqDuration(and(contains(Data.(session_str).seqType,conf.seq1),Data.(session_str).seqErrors == 0))];
        MTcorr{2} = [MTcorr{2};Data.(session_str).seqDuration(and(contains(Data.(session_str).seqType,conf.seq2),Data.(session_str).seqErrors == 0))];
        MTcorr{3} = [MTcorr{3};Data.(session_str).seqDuration(and(contains(Data.(session_str).seqType,conf.seq3),Data.(session_str).seqErrors == 0))];
        MTcorr{4} = [MTcorr{4};Data.(session_str).seqDuration(and(contains(Data.(session_str).seqType,conf.seq4),Data.(session_str).seqErrors == 0))];
        MTcorr{5} = [MTcorr{5};Data.(session_str).seqDuration(and(contains(Data.(session_str).seqType,conf.seq5),Data.(session_str).seqErrors == 0))];
        MTcorr{6} = [MTcorr{6};Data.(session_str).seqDuration(and(contains(Data.(session_str).seqType,conf.seq6),Data.(session_str).seqErrors == 0))];
        
        
        % Index of correct trials (For example: for the EXT1 sequence, the vector will contain the indexes of correct trials from 1 to a max number of trials of (30 * 64). This output needs to be associated to MTcorr and RTcorr (see below)
        vec_seq_order = seqOrder_HOME(150,[64,64,10,10,1,1],nsess,Data.(session_str).seqType,conf.seq1,conf.seq2,conf.seq3,conf.seq4,conf.seq5,conf.seq6);
        
        ORDERcorr{1} = [ORDERcorr{1};vec_seq_order(and(contains(Data.(session_str).seqType,conf.seq1),Data.(session_str).seqErrors == 0))];
        ORDERcorr{2} = [ORDERcorr{2};vec_seq_order(and(contains(Data.(session_str).seqType,conf.seq2),Data.(session_str).seqErrors == 0))];
        ORDERcorr{3} = [ORDERcorr{3};vec_seq_order(and(contains(Data.(session_str).seqType,conf.seq3),Data.(session_str).seqErrors == 0))];
        ORDERcorr{4} = [ORDERcorr{4};vec_seq_order(and(contains(Data.(session_str).seqType,conf.seq4),Data.(session_str).seqErrors == 0))];
        ORDERcorr{5} = [ORDERcorr{5};vec_seq_order(and(contains(Data.(session_str).seqType,conf.seq5),Data.(session_str).seqErrors == 0))];
        ORDERcorr{6} = [ORDERcorr{6};vec_seq_order(and(contains(Data.(session_str).seqType,conf.seq6),Data.(session_str).seqErrors == 0))];
        
        
        % RT: Response time to the first stimulus display
        ind_temp = [1; find( (Data.(session_str).dispNum(2:end)-Data.(session_str).dispNum(1:end-1)) == -9) + 1]; % RT to the first DSP stimulus (correct or not correct)
        temp = Data.(session_str).dispRT(ind_temp);
        
        RT{1} = [RT{1};temp(contains(Data.(session_str).seqType,conf.seq1))];
        RT{2} = [RT{2};temp(contains(Data.(session_str).seqType,conf.seq2))];
        RT{3} = [RT{3};temp(contains(Data.(session_str).seqType,conf.seq3))];
        RT{4} = [RT{4};temp(contains(Data.(session_str).seqType,conf.seq4))];
        RT{5} = [RT{5};temp(contains(Data.(session_str).seqType,conf.seq5))];
        RT{6} = [RT{6};temp(contains(Data.(session_str).seqType,conf.seq6))];
        
        % RT of correct trials only
        ind_temp = [1; find( (Data.(session_str).dispNum(2:end)-Data.(session_str).dispNum(1:end-1)) == -9) + 1]; % RT to the first DSP stimulus (correct or not correct)
        temp = Data.(session_str).dispRT(ind_temp);
        
        RTcorr{1} = [RTcorr{1};temp(and(contains(Data.(session_str).seqType,conf.seq1),Data.(session_str).seqErrors == 0))];
        RTcorr{2} = [RTcorr{2};temp(and(contains(Data.(session_str).seqType,conf.seq2),Data.(session_str).seqErrors == 0))];
        RTcorr{3} = [RTcorr{3};temp(and(contains(Data.(session_str).seqType,conf.seq3),Data.(session_str).seqErrors == 0))];
        RTcorr{4} = [RTcorr{4};temp(and(contains(Data.(session_str).seqType,conf.seq4),Data.(session_str).seqErrors == 0))];
        RTcorr{5} = [RTcorr{5};temp(and(contains(Data.(session_str).seqType,conf.seq5),Data.(session_str).seqErrors == 0))];
        RTcorr{6} = [RTcorr{6};temp(and(contains(Data.(session_str).seqType,conf.seq6),Data.(session_str).seqErrors == 0))];
        
        
        % ERR: Errors committed during the execution of the sequence
        ERR{1} = [ERR{1};Data.(session_str).seqErrors(contains(Data.(session_str).seqType,conf.seq1))];
        ERR{2} = [ERR{2};Data.(session_str).seqErrors(contains(Data.(session_str).seqType,conf.seq2))];
        ERR{3} = [ERR{3};Data.(session_str).seqErrors(contains(Data.(session_str).seqType,conf.seq3))];
        ERR{4} = [ERR{4};Data.(session_str).seqErrors(contains(Data.(session_str).seqType,conf.seq4))];
        ERR{5} = [ERR{5};Data.(session_str).seqErrors(contains(Data.(session_str).seqType,conf.seq5))];
        ERR{6} = [ERR{6};Data.(session_str).seqErrors(contains(Data.(session_str).seqType,conf.seq6))];
        
        % ERRw: Proportion of wrong displays/sequence (take numDispError, assign it to 1 even if more mistakes are done for a particular display, average each 10 displays of the 150 sequences to get the accuracy)
        temp = Data.(session_str).numDispError; temp(temp>0) = 1; temp = mean(reshape(temp',10,150),1)';    % 10 is the n� of displays and 150 the number of trials(sequences) in each home session
        ERRw{1} = [ERRw{1};temp(contains(Data.(session_str).seqType,conf.seq1))];
        ERRw{2} = [ERRw{2};temp(contains(Data.(session_str).seqType,conf.seq2))];
        ERRw{3} = [ERRw{3};temp(contains(Data.(session_str).seqType,conf.seq3))];
        ERRw{4} = [ERRw{4};temp(contains(Data.(session_str).seqType,conf.seq4))];
        ERRw{5} = [ERRw{5};temp(contains(Data.(session_str).seqType,conf.seq5))];
        ERRw{6} = [ERRw{6};temp(contains(Data.(session_str).seqType,conf.seq6))];
        
    end
    
    % ERRt: total number of uncorrect responses during sequence execution. Many mistakes can be done on the same display! 
    ERRt = ERR;
    
    % Modify ERR such that the sequence is identified as entirely correct (0) or with at least one error (1)
    for nseq = 1:conf.Nseq
        ERR{nseq}(ERR{nseq}>0) = 1;
    end
    
    
    % Modify MT such that MT is the time between first and last button press (i.e.: remove RT to first DSP display from MT)
    for nseq = 1:conf.Nseq
        MT{nseq} = MT{nseq} - RT{nseq};
    end
    
    
    % Modify MTcorr such that MTcorr is the time between first and last button press (i.e.: remove RTcorr to first DSP display from MTcorr)
    for nseq = 1:conf.Nseq
        MTcorr{nseq} = MTcorr{nseq} - RTcorr{nseq};
    end
    
    
    % check for outliers in MT (values > 20 seconds)
    cont = 0;
    temp = MT;
    for nseq = 1:conf.Nseq
        
        ind_out = find((MT{nseq} > Outlier_thrshold_MT));   cont = cont + length(ind_out);
        temp{nseq}(ind_out) = [];
        MT{nseq}(ind_out) = ones(length(ind_out),1) * mean(temp{nseq});
        
    end
    
    if cont == 0
        disp([SubjectsId,': NO outliers in MT'])
    else
        disp([SubjectsId,': Found ',num2str(cont),' outliers in MT'])
    end
    
    
    % check for outliers in RT (values > 3 seconds)
    cont = 0;
    temp = RT;
    for nseq = 1:conf.Nseq
        
        ind_out = find((RT{nseq} > Outlier_thrshold_RT));   cont = cont + length(ind_out);
        temp{nseq}(ind_out) = [];
        RT{nseq}(ind_out) = ones(length(ind_out),1) * mean(temp{nseq});
        
    end
    
    if cont == 0
        disp([SubjectsId,': NO outliers in RT'])
    else
        disp([SubjectsId,': Found ',num2str(cont),' outliers in RT'])
    end
    
    
    % check for outliers in MTcorr (values > 20 seconds)
    cont = 0;
    temp = MTcorr;
    for nseq = 1:conf.Nseq
        
        ind_out = find((MTcorr{nseq} > Outlier_thrshold_MT));   cont = cont + length(ind_out);
        temp{nseq}(ind_out) = [];
        MTcorr{nseq}(ind_out) = ones(length(ind_out),1) * mean(temp{nseq});
        
    end
    
    if cont == 0
        disp([SubjectsId,': NO outliers in MTcorr'])
    else
        disp([SubjectsId,': Found ',num2str(cont),' outliers in MTcorr'])
    end
    
    
    % check for outliers in RTcorr (values > 3 seconds)
    cont = 0;
    temp = RTcorr;
    for nseq = 1:conf.Nseq
        
        ind_out = find((RTcorr{nseq} > Outlier_thrshold_RT));   cont = cont + length(ind_out);
        temp{nseq}(ind_out) = [];
        RTcorr{nseq}(ind_out) = ones(length(ind_out),1) * mean(temp{nseq});
        
    end
    
    if cont == 0
        disp([SubjectsId,': NO outliers in RTcorr'])
    else
        disp([SubjectsId,': Found ',num2str(cont),' outliers in RTcorr'])
    end
    
    
    
    %% save dataset
    save(outputDataset,'Data','MT','MTcorr','RTcorr','ORDERcorr','ERR','ERRw','ERRt','RT')
    
    % clear variables
    clear MT RT ERR ERRt ERRw MTcorr RTcorr ORDERcorr
    
end  % end loop over subjects











