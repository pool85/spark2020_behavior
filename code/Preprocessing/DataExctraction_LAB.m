% DataExctraction_LAB.m
%
% SPARK PROJECT 2020. DSP task - Lab-based Sessions
% This script extract the behavioural data from the participants (contained
% in the .csv files in ../raw/lab) and save them into a matlab structure
% (in ../extracted/lab)
%
% Copyright (c) 2020 UNIL
% Paolo Ruggeri (paolo.ruggeri@unil.ch)
% Jenifer Miehlbradt (jenifer.miehlbradt@unil.ch)
clc
clear all

%% Initialize the script
addpath('..');
initAnalysis

%% Find file names and subject IDs
[csvFileNames, subjects] = getSubjectIds(conf.RAW_FOLDER_LAB);

nSubjects = length(subjects); % number of subjects


%% Data extraction
for i = 1:nSubjects
    
    SubjectsId = subjects{i};
    
    Sessions = csvFileNames((contains(csvFileNames,SubjectsId)));
    
    nSessions = length(Sessions);
    
    disp(['From the number of raw files, it seems that subject "', SubjectsId,'" has performed ',num2str(nSessions),' laboratory sessions.'])
    
    outputDataset = [ conf.EXTRACTED_FOLDER_LAB filesep SubjectsId filesep SubjectsId '.mat'];
    
    
    for j = 1:nSessions
        
        SessionsId = Sessions{j};
        
        inputDataset  = [ conf.RAW_FOLDER_LAB filesep SessionsId '.csv'];
        
        if ~exist(inputDataset, 'file')
            error('Output from psychopy does not exist');
        end
        
        
        % Create folder if does not exist
        createFolderPath(outputDataset);
        
        % 1: Load the variables of interest from psychopy output
        table = readtable(inputDataset);
        sessionCSV = ['Session_',num2str(table.session(1))];
        
        
        for nepochs = 1:conf.LAB_Epochs
            epoch_str = ['E_',num2str(nepochs)];
            
            % num errors sequence
            temp = table.num_errors_sequence(table.Epochs_thisN == (nepochs-1));
            ind_temp = ~isnan(temp);
            Data.(sessionCSV).(epoch_str).seqErrors = temp(ind_temp);
            
            % sequence duration with correct resp
            temp = table.Sequence_Duration(table.Epochs_thisN == (nepochs-1));
            ind_temp = ~isnan(temp);
            Data.(sessionCSV).(epoch_str).seqDuration = temp(ind_temp);
            
            % sequence duration (with delay caused by errors)
            temp = table.Sequence_Duration_all(table.Epochs_thisN == (nepochs-1));
            ind_temp = ~isnan(temp);
            Data.(sessionCSV).(epoch_str).seqDurationAll = temp(ind_temp);
            
            % sequence type (with delay caused by errors)
            temp = table.condsFile(table.Epochs_thisN == (nepochs-1));
            Data.(sessionCSV).(epoch_str).seqType = temp(ind_temp);
            
            % RT for each display, (NB: the r.t is of the correct resp. only)
            temp = table.key_resp_2_rt(table.Epochs_thisN == (nepochs-1));
            ind_temp = ~isnan(temp);
            Data.(sessionCSV).(epoch_str).RT = temp(ind_temp);
            
            % num errors per display
            temp = table.num_errors(table.Epochs_thisN == (nepochs-1));
            ind_temp = ~isnan(temp);
            Data.(sessionCSV).(epoch_str).dispErrors = temp(ind_temp);
            
        end
        
        
    end
    
    save(outputDataset,'Data')
    clear Data
    
end